<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth:sanctum'])->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/login', [\App\Http\Controllers\AuthController::class, 'login']);
Route::post('/login/confirm', [\App\Http\Controllers\AuthController::class, 'confirmCode']);


Route::middleware('auth:web')->group(function (){
    Route::get('/cabinet/user/get', [\App\Http\Controllers\CabinetController::class, 'getUserCabinet']);
    Route::post('/cabinet/user/update', [\App\Http\Controllers\CabinetController::class, 'updateUserCabinet']);


    Route::get('/post/get', [\App\Http\Controllers\PostController::class, 'getPost']);
    Route::get('/post/my_posts', [\App\Http\Controllers\PostController::class, 'getMyPosts']);
    Route::get('/post/form/create', [\App\Http\Controllers\PostController::class, 'formPostCreate']);
    Route::get('/post/form/update', [\App\Http\Controllers\PostController::class, 'formPostUpdate']);
    Route::get('/post/form/search_residential', [\App\Http\Controllers\PostController::class, 'getResidentialBySearch']);
    Route::post('/post/create', [\App\Http\Controllers\PostController::class, 'createPost']);
    Route::post('/post/update', [\App\Http\Controllers\PostController::class, 'updatePost']);
    Route::post('/post/delete', [\App\Http\Controllers\PostController::class, 'deletePost']);
    Route::get('/post/list', [\App\Http\Controllers\PostController::class, 'getPostsList']);
    Route::get('/post/list/map', [\App\Http\Controllers\PostController::class, 'getPostsListMap']);

    Route::get('/favorite/post/add', [\App\Http\Controllers\PostController::class, 'addFavorite']);
    Route::get('/favorite/post/delete', [\App\Http\Controllers\PostController::class, 'deleteFavorite']);
    Route::get('/favorite/list', [\App\Http\Controllers\PostController::class, 'listFavorite']);


    Route::get('/promotion/get', [\App\Http\Controllers\PromotionController::class, 'getPromotionTypes']);
    Route::get('/promotion/phrases/get', [\App\Http\Controllers\PromotionController::class, 'getPromotionPhrases']);
    Route::post('/promotion/set', [\App\Http\Controllers\PromotionController::class, 'setPromotionsPost']);


    Route::get('/filter/types/get', [\App\Http\Controllers\FilterController::class, 'getFilterTypes']);
    Route::get('/filter/get', [\App\Http\Controllers\FilterController::class, 'getFilter']);
    Route::post('/filter/set', [\App\Http\Controllers\FilterController::class, 'setFilter']);


    Route::get('/support/centers/get', [\App\Http\Controllers\SupportController::class, 'getMFC']);
    Route::get('/support/chat/user/get', [\App\Http\Controllers\SupportController::class, 'getChat']);
    Route::post('/support/chat/user/send', [\App\Http\Controllers\SupportController::class, 'sendUserMessage']);


    Route::get('/notary/user/get', [\App\Http\Controllers\NotaryController::class, 'getNotaries']);
});

Route::middleware('auth:admin')->group(function (){
    Route::get('/cabinet/admin/get', [\App\Http\Controllers\CabinetController::class, 'getAdminCabinet']);
    Route::post('/cabinet/admin/update', [\App\Http\Controllers\CabinetController::class, 'updateAdminCabinet']);

    Route::get('/moderate/post/list', [\App\Http\Controllers\PostController::class, 'listModerPosts']);
    Route::get('/moderate/post/get', [\App\Http\Controllers\PostController::class, 'getModerPost']);
    Route::post('/moderate/post/accept', [\App\Http\Controllers\PostController::class, 'acceptPost']);
    Route::post('/moderate/post/reject', [\App\Http\Controllers\PostController::class, 'rejectPost']);
    Route::post('/moderate/post/reject/phrases', [\App\Http\Controllers\PostController::class, 'rejectPostPhrases']);


    Route::get('/user/list', [\App\Http\Controllers\UserController::class, 'listUsers']);
    Route::get('/user/list/filter', [\App\Http\Controllers\UserController::class, 'listUsersByName']);
    Route::get('/user/get', [\App\Http\Controllers\UserController::class, 'getUser']);

    Route::get('/user/blacklist/get', [\App\Http\Controllers\UserController::class, 'getBlackList']);
    Route::get('/user/blacklist/filter', [\App\Http\Controllers\UserController::class, 'getBlackListByName']);
    Route::post('/user/add_to_blacklist', [\App\Http\Controllers\UserController::class, 'addToBlackList']);
    Route::post('/user/remove_to_blacklist', [\App\Http\Controllers\UserController::class, 'removeFromBlackList']);


    Route::get('/notary/admin/list', [\App\Http\Controllers\NotaryController::class, 'getNotaries']);
    Route::get('/notary/admin/get', [\App\Http\Controllers\NotaryController::class, 'getNotary']);
    Route::post('/notary/admin/add', [\App\Http\Controllers\NotaryController::class, 'addNotary']);
    Route::post('/notary/admin/update', [\App\Http\Controllers\NotaryController::class, 'updateNotary']);
    Route::post('/notary/admin/delete', [\App\Http\Controllers\NotaryController::class, 'deleteNotary']);


    Route::get('/support/chat/admin/get', [\App\Http\Controllers\SupportController::class, 'getChat']);
    Route::post('/support/chat/admin/send', [\App\Http\Controllers\SupportController::class, 'sendAdminMessage']);

});

Route::middleware('auth:builder')->group(function (){
    Route::get('/cabinet/builder/get', [\App\Http\Controllers\CabinetController::class, 'getBuilderCabinet']);
    Route::post('/cabinet/builder/update', [\App\Http\Controllers\CabinetController::class, 'updateBuilderCabinet']);


    Route::get('/residential/get', [\App\Http\Controllers\ResidentialController::class, 'listResidentials']);
    Route::get('/residential/builder/get', [\App\Http\Controllers\ResidentialController::class, 'getBuilderResidentials']);
    Route::get('/residential/form/create', [\App\Http\Controllers\ResidentialController::class, 'createFormResidential']);
    Route::get('/residential/form/update', [\App\Http\Controllers\ResidentialController::class, 'updateFormResidential']);
    Route::get('/residential/advantages/get', [\App\Http\Controllers\ResidentialController::class, 'getFormAdvantages']);

    Route::post('/residential/create', [\App\Http\Controllers\ResidentialController::class, 'createResidential']);
    Route::post('/residential/update', [\App\Http\Controllers\ResidentialController::class, 'updateResidential']);
    Route::post('/residential/advantages/set', [\App\Http\Controllers\ResidentialController::class, 'setAdvantages']);
    Route::post('/residential/delete', [\App\Http\Controllers\ResidentialController::class, 'deleteResidential']);

    Route::get('/residential/grid/get', [\App\Http\Controllers\GridController::class, 'getGrid']);
    Route::get('/residential/grid/cell/get', [\App\Http\Controllers\GridController::class, 'getGridCell']);
    Route::post('/residential/grid/cell/write', [\App\Http\Controllers\GridController::class, 'writeCell']);
    Route::post('/residential/grid/cell/clear', [\App\Http\Controllers\GridController::class, 'clearCell']);
    Route::get('/residential/grid/request/get', [\App\Http\Controllers\GridController::class, 'getGridRequests']);




});
