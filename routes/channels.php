<?php

use App\Models\Support;
use Illuminate\Support\Facades\Broadcast;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('supports.id', function ($user, $id) {
//    return $user->id === Support::findOrNew('id', $id)->user_id;
    return $user->id === $user->id;
});
