<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PostModerate extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $post;
    private $moderate_status;
    public function __construct($post, int $moderate_status)
    {
        $this->post = $post;
        $this->moderate_status = $moderate_status;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->moderate_status == 1)
            return $this->view('emails.posts.post_rejected')
                ->subject('Отказано в публикации');
        else
            return $this->view('emails.posts.post_accepted')
                ->subject('Принято на публикацию');

    }
}
