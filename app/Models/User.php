<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = [
        'email',
        'phone',
        'firstname',
        'lastname',
        'agent_email',
        'agent_phone',
        'agent_firstname',
        'agent_lastname',
        'notifications',
        'signed_before',
        'blacklist',
        'turn_on_agent'
    ];

    public $timestamps = false;

    public function getSignedBeforeAttribute($value)
    {
        return new Carbon($value);
    }
    public function posts()
    {
        return $this->hasMany(Post::class);
    }


    static function updateCabinet(Request $request)
    {
        if (isset($request->image))
        {
            $user = User::where('id', Auth::user()->id)->first();
            if(isset($user->image))
                Storage::delete($user->image);

            $image = explode('/', $request->file('image')->store('images/users'));
            User::where('id', Auth::user()->id)->update(['image' => 'images/users/' . $image[count($image)-1]]);
        }
        User::where('id', Auth::user()->id)->update([
            'email' => $request->email,
            'phone' => $request->phone,
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'agent_email' => $request->agent_email,
            'agent_phone' => $request->agent_phone,
            'agent_firstname' => $request->agent_firstname,
            'agent_lastname' => $request->agent_lastname,
            'notifications' => $request->notifications,
            'signed_before' => $request->signed_before,
            'turn_on_agent' => $request->turn_on_agent
        ]);
    }
}
