<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Filter extends Model
{
    use HasFactory;

    static function setFilter(Request $request)
    {
        Filter::updateOrInsert([
            'user_id' => Auth::user()->id,
            'post_type_id' => $request->post_type_id
        ],[
            'district' => $request->district,
            'micro_district' => $request->micro_district,
            'count_room' => $request->count_room,
            'price_from' => $request->price_from,
            'price_to' => $request->price_to,
            'square_from' => $request->square_from,
            'square_to' => $request->square_to,
            'flat_type' => $request->flat_type,
            'mortage' => $request->mortage,
            'flat_state' => $request->state,
            'save_filter' => $request->save_filter,
        ]);
    }
}
