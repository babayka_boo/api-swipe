<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class Admin extends Authenticatable
{
    use HasFactory;
    protected $fillable = [
        'email',
        'phone',
        'firstname',
        'lastname',
        'image'
    ];
    public $timestamps = false;

    static function updateCabinet(Request $request)
    {
        if (isset($request->image))
        {
            $admin = Admin::where('id', Auth::user()->id)->first();
            if(isset($admin->image))
                Storage::delete($admin->image);

            $image = explode('/', $request->file('image')->store('images/admins'));
            Admin::where('id', Auth::user()->id)->update([
                'image' => 'images/admins/' . $image[count($image)-1],
            ]);
        }

        Admin::where('id', Auth::user()->id)->update([
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'phone' => $request->phone,
            'email' => $request->email,
        ]);
    }
}
