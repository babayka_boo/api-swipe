<?php

namespace App\Models;

use App\Mail\PostByFilter;
use App\Mail\PostModerate;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class Post extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'address',
        'district',
        'micro_district',
        'residential_id',
        'document_owner',
        'flat_type',
        'count_room',
        'flat_state',
        'square',
        'square_kitchen',
        'balcony',
        'type_heating',
        'commission',
        'connection_type',
        'desc',
        'gallery_id',
        'mortage',
        'price',
        'payment_type',
        'post_type_id',
        'views',
        'moderate_accept',
        'moderate_message',
    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    static function sendMessageByFilter($post)
    {
        $filters = Filter::where('post_type_id', $post->post_type)->where('save_filter', 10)->get();
        foreach ($filters as $filter)
        {
            if (isset($filter->district) && $filter->district != $post->district)
                continue;
            if (isset($filter->micro_district) && $filter->micro_district != $post->micro_district)
                continue;
            if (isset($filter->count_room) && $filter->count_room != $post->count_room)
                continue;
            if (isset($filter->price_from) && $filter->price_from > $post->price)
                continue;
            if (isset($filter->price_to) && $filter->price_to < $post->price)
                continue;
            if (isset($filter->square_from) && $filter->square_from > $post->square)
                continue;
            if (isset($filter->square_to) && $filter->square_to < $post->square)
                continue;
            if (isset($filter->flat_type) && $filter->flat_type != $post->flat_type)
                continue;
            if (isset($filter->mortage) && $filter->mortage != $post->mortage)
                continue;
            if (isset($filter->flat_state) && $filter->flat_state != $post->flat_state)
                continue;
            $user = User::where('id', $filter->user_id)->first();
            Mail::to($user->turn_on_agent == 10 ? $user->agent_email : $user->email)->send(new PostByFilter($post));
        }
    }

    static function createPost(Request $request) : int
    {
        $gallery_id = null;
        if (isset($request->photos))
            $gallery_id = Gallery::saveImages($request, 'images/posts');

        Post::insert([
            'user_id' => Auth::user()->id,
            'address' => $request->address,
            'district' => $request->district,
            'micro_district' => $request->micro_district,
            'residential_id' => $request->residential_id,
            'document_owner' => $request->document_owner,
            'flat_type' => $request->flat_type,
            'count_room' => $request->count_room,
            'flat_state' => $request->flat_state,
            'square' => $request->square,
            'square_kitchen' => $request->square_kitchen,
            'balcony' => $request->balcony,
            'heating_type' => $request->heating_type,
            'commission' => $request->commission,
            'mortage' => $request->mortage,
            'connection_type' => $request->connection_type,
            'desc' => $request->desc,
            'gallery_id' => $gallery_id,
            'price' => $request->price,
            'payment_type' => $request->payment_type,
            'post_type_id' => $request->post_type,
            'views' => 0,
        ]);
        $post = Post::where('id', Post::max('id'))->first();

        foreach($request->plans as $plan)
        {
            PostPlan::insert([
                'plan_id' => $plan,
                'post_id' => $post->id
            ]);
        }
        GridRequest::addRequest($post->residential_id, $post->id);

        Post::sendMessageByFilter($post);
        return $post->id;
    }
    static function updatePost(Request $request, int $post_id) : void
    {
        $gallery_id = Post::where('id', $post_id)->first()->gallery_id;
        if (isset($request->delete_photos)) {
            foreach ($request->delete_photos as $position){
                    Storage::delete(Gallery::where('gallery', $gallery_id)->where('position', $position)->first()->url);
                    Gallery::where('gallery', $gallery_id)->where('position', $position)->delete();
            }
        }
        if (isset($request->photos))
            Gallery::saveImages($request, 'images/posts', $gallery_id);

        Post::where('id', $post_id)->update([
            'address' => $request->address,
            'district' => $request->district,
            'micro_district' => $request->micro_district,
            'residential_id' => $request->residential_id,
            'document_owner' => $request->document_owner,
            'flat_type' => $request->flat_type,
            'count_room' => $request->count_room,
            'flat_state' => $request->flat_state,
            'square' => $request->square,
            'square_kitchen' => $request->square_kitchen,
            'balcony' => $request->balcony,
            'heating_type' => $request->heating_type,
            'commission' => $request->commission,
            'mortage' => $request->mortage,
            'connection_type' => $request->connection_type,
            'desc' => $request->desc,
            'price' => $request->price,
            'payment_type' => $request->payment_type,
            'post_type_id' => $request->post_type,
            'moderate_accept' => 5
        ]);

        PostPlan::where('post_id', $post_id)->delete();
        foreach($request->plans as $plan)
        {
            PostPlan::insert([
                'plan_id' => $plan,
                'post_id' => $post_id
            ]);
        }
    }
    static function deletePost(int $post_id)
    {
        PostPromotion::where('post_id', $post_id)->delete();
        PostPlan::where('post_id', $post_id)->delete();
        Favorite::where('post_id', $post_id)->delete();
        Grid::where('post_id', $post_id)->delete();
        GridRequest::where('post_id', $post_id)->delete();
        $gallery_id = Post::where('id', $post_id)->first()->gallery_id;
        if($gallery_id != null)
            Gallery::deleteImages($gallery_id);

        Post::where('id', $post_id)->delete();
    }

    static function acceptPost(int $post_id)
    {
        $post = Post::where('id', $post_id)->first();
        Post::where('id', $post_id)->update([
           'moderate_accept' => 10
        ]);
        $user = User::where('id', $post->user_id)->first();
        Mail::to($user->turn_on_agent == 10 ? $user->agent_email : $user->email)->send(new PostModerate($post, 10));
    }
    static function rejectPost(int $post_id, string $message)
    {
        $post = Post::where('id', $post_id)->first();
        Post::where('id', $post_id)->update([
           'moderate_accept' => 1,
           'moderate_message' => $message,
        ]);
        $user = User::where('id', $post->user_id)->first();
        Mail::to($user->turn_on_agent == 10 ? $user->agent_email : $user->email)->send(new PostModerate($post, 1));
    }

}
