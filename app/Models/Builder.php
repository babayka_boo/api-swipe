<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class Builder extends Model
{
    use HasFactory;

    protected $fillable = [
        'email',
        'phone',
        'firstname',
        'lastname',
        'image'
    ];
    public $timestamps = false;

    static function updateCabinet(Request $request)
    {
        if (isset($request->image))
        {
            $builder = Builder::where('id', Auth::user()->id)->first();
            if(isset($builder->image))
                Storage::delete($builder->image);

            $image = explode('/', $request->file('image')->store('images/builders'));
            Builder::where('id', Auth::user()->id)->update([
                'image' => 'images/builders/' . $image[count($image)-1],
            ]);
        }

        Builder::where('id', Auth::user()->id)->update([
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'phone' => $request->phone,
            'email' => $request->email,
        ]);
    }
}
