<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Advantage extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'name',
    ];

    public function residentials()
    {
        return $this->belongsToMany(Residential::class);
    }
    static function setAdvantages(Request $request, int $residential_id)
    {
        ResidentialAdvantage::where('residential_id', $residential_id)->delete();
        foreach($request->active_advantages as $advantage)
            ResidentialAdvantage::insert([
               'residential_id' => $residential_id,
               'advantage_id' => $advantage
            ]);
    }
}
