<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class Gallery extends Model
{
    use HasFactory;

    static function saveImages(Request $request, string $url, int $gallery_id = null)
    {
        if($gallery_id == null)
            $gallery_id = Gallery::max('gallery') + 1;

        foreach ($request->files->all()['photos'] as $position => $img) {
            $path = explode('/', $request->file('photos.'.$position)->store($url));

            $old_gallery = Gallery::where('gallery', $gallery_id)->where('position', $position)->first();
            if($old_gallery != null){
                Storage::delete($old_gallery->url);
                Gallery::where('gallery', $gallery_id)->where('position', $position)->delete();
            }

            Gallery::insert([
                'gallery' => $gallery_id,
                'position' => $position,
                'url' => 'images/posts/'.$path[count($path) - 1]
            ]);
        }
        return $gallery_id;
    }

    static function deleteImages(int $gallery_id)
    {
        $gallery = Gallery::where('gallery', $gallery_id)->get();
        if ($gallery[0] != null)
        {
            foreach (Gallery::where('gallery', $gallery->gallery)->get() as $image) {
                Storage::delete(Gallery::where('gallery', $gallery->gallery)->where('position', $image->position)->first()->url);
                Gallery::where('gallery', $gallery->gallery)->where('position', $image->position)->delete();
            }
        }
    }
}
