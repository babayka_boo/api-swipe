<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class Notary extends Model
{
    use HasFactory;
    protected $fillable = [
        'email',
        'phone',
        'firstname',
        'lastname',
        'image'
    ];
    public $timestamps = false;

    static function addNotary(Request $request) : int
    {
        $image = null;
        if (isset($request->image))
            $image = explode('/', $request->file('image')->store('images/notaries'));

        Notary::insert([
           'firstname' => $request->firstname,
           'lastname' => $request->lastname,
           'phone' => $request->phone,
           'email' => $request->email,
           'image' => isset($image) ? 'images/notaries/' . $image[count($image)-1] : null,
        ]);

        return Notary::max('id');
    }

    static function updateNotary(Request $request, int $notary_id)
    {
        if (isset($request->image))
        {
            $notary = Notary::where('id', $notary_id)->first();
            if(isset($notary->image))
                Storage::delete($notary->image);

            $image = explode('/', $request->file('image')->store('images/notaries'));
            Notary::where('id', $notary_id)->update(['image' => 'images/notaries/' . $image[count($image)-1]]);
        }

        Notary::where('id', $notary_id)->update([
           'firstname' => $request->firstname,
           'lastname' => $request->lastname,
           'phone' => $request->phone,
           'email' => $request->email
        ]);
    }

    static function deleteNotary(int $notary_id)
    {
        $notary = Notary::where('id', $notary_id)->first();
        if (isset($notary->image))
            Storage::delete($notary->image);
        Notary::where('id', $notary_id)->delete();
    }
}
