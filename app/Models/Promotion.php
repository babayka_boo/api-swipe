<?php

namespace App\Models;

use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Promotion extends Model
{
    use HasFactory;


    static function setPromotions(Request $request)
    {
        $date = new DateTime();
        $date = $date->modify('+1 month')->format('Y-m-d');
        foreach($request->promotions as $id => $value)
        {
            PostPromotion::updateOrInsert([
                'post_id' => $request->post_id,
                'promotion_id' => $id
            ],
            [
                'value' => $value,
                'date_end' => $date
            ]);
        }
    }
}
