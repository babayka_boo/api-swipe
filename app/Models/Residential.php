<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use PhpParser\Comment\Doc;

class Residential extends Model
{
    use HasFactory;
    protected $fillable = [
        'builder_id',
        'coordinates',
        'name',
        'desc',
        'status',
        'residential_type',
        'house_class',
        'technology_build',
        'territory',
        'distance_sea',
        'communal',
        'height_ceiling',
        'gas',
        'heating',
        'sewerage',
        'water',
        'registration',
        'mortage',
        'flat_type',
        'sum_contract',
        'center_firstname',
        'center_lastname',
        'center_phone',
        'center_email',
        'gallery_id',
        'count_sections'
    ];
    public $timestamps = false;
    public function advantages()
    {
        return $this->belongsToMany(Advantage::class);
    }

    static function saveFiles(Request $request, int $residential_id)
    {
        $files = $request->files->all();
        if (isset($files['excels']))
        {
            foreach ($files['excels'] as $key => $file) {
                $path = explode('/', $request->file('excels.'.$key)->store('images/residentials/excels'));
                Document::insert([
                    'residential_id' => $residential_id,
                    'url' => 'images/residentials/excels/'.$path[count($path) - 1],
                    'type' => 'excel'
                ]);
            }
        }
        if (isset($files['pdf']))
        {
            foreach ($files['pdf'] as $key => $file) {
                $path = explode('/', $request->file('pdf.'.$key)->store('images/residentials/pdf'));
                Document::insert([
                    'residential_id' => $residential_id,
                    'url' => 'images/residentials/pdf/'.$path[count($path) - 1],
                    'type' => 'pdf'
                ]);
            }
        }
    }

    static function createResidential(Request $request) : int
    {
        $gallery_id = null;
        if (isset($request->photos))
            $gallery_id = Gallery::saveImages($request, 'images/residentials/photos');

        Residential::insert([
            'builder_id' => Auth::user()->id,
            'coordinates' => $request->coordinates,
            'name' => $request->name,
            'desc' => $request->desc,
            'status' => $request->residential_status,
            'residential_type' => $request->residential_type,
            'house_class' => $request->house_class,
            'technology_build' => $request->technology_build,
            'territory' => $request->territory,
            'distance_sea' => $request->distance_sea,
            'communal' => $request->communal_payments,
            'height_ceiling' => $request->height_ceiling,
            'gas' => $request->gas_types,
            'heating' => $request->heating_type,
            'sewerage' => $request->sewerage_types,
            'water' => $request->water_types,
            'registration' => $request->registration,
            'mortage' => $request->mortage,
            'flat_type' => $request->flat_type,
            'sum_contract' => $request->sum_contract,
            'center_firstname' => $request->center_firstname,
            'center_lastname' => $request->center_lastname,
            'center_phone' => $request->center_phone,
            'center_email' => $request->center_email,
            'gallery_id' => $gallery_id
        ]);
        $residential_id = Residential::max('id');
        Residential::saveFiles($request, $residential_id);

        return $residential_id;
    }

    static function updateResidential(Request $request, $residential_id)
    {
        $gallery_id = Residential::where('id', $residential_id)->first()->gallery_id;
        if (isset($request->delete_photos)) {
            foreach ($request->delete_photos as $position){
                Storage::delete(Gallery::where('gallery', $gallery_id)->where('position', $position)->first()->url);
                Gallery::where('gallery', $gallery_id)->where('position', $position)->delete();
            }
        }
        if (isset($request->photos))
            Gallery::saveImages($request, 'images/residentials/photos', $gallery_id);

        Residential::where('id', $residential_id)->update([
            'coordinates' => $request->coordinates,
            'name' => $request->name,
            'desc' => $request->desc,
            'status' => $request->residential_status,
            'residential_type' => $request->residential_type,
            'house_class' => $request->house_class,
            'technology_build' => $request->technology_build,
            'territory' => $request->territory,
            'distance_sea' => $request->distance_sea,
            'communal' => $request->communal_payments,
            'height_ceiling' => $request->height_ceiling,
            'gas' => $request->gas_types,
            'heating' => $request->heating_type,
            'sewerage' => $request->sewerage_types,
            'water' => $request->water_types,
            'registration' => $request->registration,
            'mortage' => $request->mortage,
            'flat_type' => $request->flat_type,
            'sum_contract' => $request->sum_contract,
            'center_firstname' => $request->center_firstname,
            'center_lastname' => $request->center_lastname,
            'center_phone' => $request->center_phone,
            'center_email' => $request->center_email,
        ]);

        if(isset($request->delete_documents))
        {
            foreach ($request->delete_documents as $doc_id)
            {
                Storage::delete(Document::where('id', $doc_id)->first()->url);
                Document::where('id', $doc_id)->delete();
            }
        }
        Residential::saveFiles($request, $residential_id);

    }

    static function deleteResidential(int $residential_id)
    {
        ResidentialAdvantage::where('residential_id', $residential_id)->delete();
        foreach(Post::where('residential_id', $residential_id)->get() as $post)
            Post::deletePost($post->id);
        Grid::where('residential_id', $residential_id)->delete();
        GridRequest::where('residential_id', $residential_id)->delete();

        foreach (Document::where('residential_id', $residential_id)->get() as $doc)
        {
            Storage::delete($doc->url);
            Document::where('id', $doc->id)->delete();
        }

        Residential::where('id', $residential_id)->delete();
    }
}
