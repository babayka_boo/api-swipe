<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Grid extends Model
{
    use HasFactory;

    static function writeCell(Request $request)
    {
        Grid::insert([
            'post_id' => $request->post_id,
            'row' => $request->row,
            'col' => $request->col,
            'section' => $request->section,
            'residential_id' => $request->residential_id
        ]);
        GridRequest::deleteRequest($request->residential_id, $request->post_id);
    }

    static function clearCell(Request $request)
    {
        $cell = Grid::where('residential_id', $request->residential_id)
            ->where('section', $request->section)
            ->where('row', $request->row)
            ->where('col', $request->col)
            ->first();
        if (isset($cell) && isset($cell->post_id))
            GridRequest::addRequest($cell->residential_id, $cell->post_id);

        Grid::where('residential_id', $request->residential_id)
            ->where('section', $request->section)
            ->where('row', $request->row)
            ->where('col', $request->col)
            ->delete();

    }
}
