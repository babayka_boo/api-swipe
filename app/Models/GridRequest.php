<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GridRequest extends Model
{
    use HasFactory;

    static function addRequest(int $residential_id, int $post_id)
    {
        GridRequest::insert([
            'residential_id' => $residential_id,
            'post_id' => $post_id
        ]);
    }

    static function deleteRequest(int $residential_id, int $post_id)
    {
        GridRequest::where('residential_id', $residential_id)
        ->where('post_id', $post_id)
        ->delete();
    }
}
