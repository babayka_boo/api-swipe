<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserCabinetResource;
use App\Http\Resources\UserResource;
use App\Models\Admin;
use App\Models\Builder;
use App\Models\Residential;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class CabinetController extends Controller
{
    /**
     * @OA\Get(path="/cabinet/user/get",
     *   tags={"Личный кабинет"},
     *   operationId="CabinetUserGet",
     *   summary="Получить данные кабинета пользователя",
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "contacts": {
     *                      "firstname": "User",
     *                      "lastname": "User",
     *                      "phone": "0663333333",
     *                      "email": "luckymr304@gmail.com",
     *                      "image": "http://swipe.com/images/users",
     *                      },
     *                      "agent_contacts": {
     *                      "firstname": "User",
     *                      "lastname": "User",
     *                      "phone": "0663333333",
     *                      "email": "luckymr304@gmail.com"
     *                      },
     *                      "signed_before": "01.03.2023",
     *                      "notifications": 10,
     *                      "turn_on_agent": 0
     *              },
     *          ),
     *        }
     *    ),
     * )
     */
    public function getUserCabinet()
    {
        return response()->json(new UserCabinetResource(User::where('id', Auth::user()->id)->first()));
    }

    /**
     * @OA\Post(path="/cabinet/user/update",
     *   tags={"Личный кабинет"},
     *   operationId="updateUserCabinet",
     *   summary="Обновить данные кабинета пользователя",
     *     @OA\RequestBody(
     *     description="",
     *              @OA\JsonContent(
     *             @OA\Property(property="email", type="string", example="lucky4@gmail.com"),
     *             @OA\Property(property="phone", type="string", example="0952266780"),
     *             @OA\Property(property="firstname", type="string", example="John"),
     *             @OA\Property(property="lastname", type="string", example="Washingthon"),
     *             @OA\Property(property="image", type="string", example="file"),
     *             @OA\Property(property="agent_email", type="string", example="lucky4@gmail.com"),
     *             @OA\Property(property="agent_phone", type="string", example="0952266780"),
     *             @OA\Property(property="agent_firstname", type="string", example="John"),
     *             @OA\Property(property="agent_lastname", type="string", example="Washingthon"),
     *             @OA\Property(property="notifications", type="integer", example="10"),
     *             @OA\Property(property="turn_on_agent", type="integer", example="1")
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 200,
     *                  "message": "Cabinet updated",
     *              }
     *          ),
     *        }
     *    ),
     *   @OA\Response(
     *      response="301",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                   "status": 301,
     *                   "message": "Email already exist"
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function updateUserCabinet(Request $request)
    {
        $user = User::where('id', Auth::user()->id)->first();
        if (User::where('email', '!=', $user->email)->where('email', $request->email)->first() != null)
            return response()->json(['status' => 301, 'message' => 'Email already exist']);

        User::updateCabinet($request);
        return response()->json(['status' => 200, 'message' => 'Cabinet updated']);
    }

    /**
     * @OA\Get(path="/cabinet/admin/get",
     *   tags={"Личный кабинет"},
     *   operationId="getAdminCabinet",
     *   summary="Получить данные кабинета админа",
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "firstname": "Admin",
     *                  "lastname": "LTE",
     *                  "email": "admin@gmail.com",
     *                  "phone": "0994567891",
     *                  "image": "http://swipe.com/images/admins/1.png"
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function getAdminCabinet()
    {
        return response()->json(new UserResource(Admin::where('id', Auth::user()->id)->first()));
    }

    /**
     * @OA\Post(path="/cabinet/admin/update",
     *   tags={"Личный кабинет"},
     *   operationId="updateAdminCabinet",
     *   summary="Обновить данные кабинета админа",
     *     @OA\RequestBody(
     *     description="",
     *              @OA\JsonContent(
     *              @OA\Property(property="email", type="string", example="lucky4@gmail.com"),
     *             @OA\Property(property="phone", type="string", example="0952266780"),
     *             @OA\Property(property="firstname", type="string", example="John"),
     *             @OA\Property(property="lastname", type="string", example="Washingthon"),
     *             @OA\Property(property="image", type="string", example="file"),
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 200,
     *                  "message": "Cabinet updated",
     *              }
     *          ),
     *        }
     *    ),
     *   @OA\Response(
     *      response="301",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                   "status": 301,
     *                   "message": "Email already exist"
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function updateAdminCabinet(Request $request)
    {
        $admin = Admin::where('id', Auth::user()->id)->first();
        if (Admin::where('email', '!=', $admin->email)->where('email', $request->email)->first() != null)
            return response()->json(['status' => 301, 'message' => 'Email already exist']);

        Admin::updateCabinet($request);
        return response()->json(['status' => 200, 'message' => 'Cabinet updated']);
    }

    /**
     * @OA\Get(path="/cabinet/builder/get",
     *   tags={"Личный кабинет"},
     *   operationId="getBuilderCabinet",
     *   summary="Получить данные кабинета застройщика",
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "builder": {
     *                      "id": 1,
     *                      "firstname": "Builder",
     *                      "lastname": "Builder",
     *                      "phone": "0662222222",
     *                      "email": "luckymr304@gmail.com",
     *                      "image": "file"
     *                  },
     *                  "has_residential": true
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function getBuilderCabinet()
    {
        if (Residential::where('builder_id', Auth::user()->id)->first() == null)
            return response()->json(['builder' => new UserResource(Builder::where('id', Auth::user()->id)->first()), 'has_residential' => false]);

        return response()->json(['builder' => new UserResource(Builder::where('id', Auth::user()->id)->first()), 'has_residential' => true]);
    }

    /**
     * @OA\Post(path="/cabinet/builder/update",
     *   tags={"Личный кабинет"},
     *   operationId="updateBuilderCabinet",
     *   summary="Обновить данные кабинета застройщика",
     *     @OA\RequestBody(
     *     description="",
     *              @OA\JsonContent(
     *             @OA\Property(property="email", type="string", example="lucky4@gmail.com"),
     *             @OA\Property(property="phone", type="string", example="0952266780"),
     *             @OA\Property(property="firstname", type="string", example="John"),
     *             @OA\Property(property="lastname", type="string", example="Washingthon"),
     *             @OA\Property(property="image", type="string", example="file"),
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 200,
     *                  "message": "Cabinet updated",
     *              }
     *          ),
     *        }
     *    ),
     *   @OA\Response(
     *      response="301",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                   "status": 301,
     *                   "message": "Email already exist"
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function updateBuilderCabinet(Request $request)
    {
        $builder = Builder::where('id', Auth::user()->id)->first();
        if (Builder::where('email', '!=', $builder->email)->where('email', $request->email)->first() != null)
            return response()->json(['status' => 301, 'message' => 'Email already exist']);

        Builder::updateCabinet($request);
        return response()->json(['status' => 200, 'message' => 'Cabinet updated']);

    }
}
