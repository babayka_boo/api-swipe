<?php

namespace App\Http\Controllers;

use App\Http\Resources\ResidentialResource;
use App\Models\Advantage;
use App\Models\Expression;
use App\Models\Favorite;
use App\Models\Residential;
use App\Models\ResidentialAdvantage;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class ResidentialController extends Controller
{
    private function getFormParameters() : array
    {
        $data['infrastructure']['residential_status'] = [];
        foreach(Expression::where('type', 'residential_status')->get() as $type)
            $data['infrastructure']['residential_status'][$type->const_id] = $type->text;

        $data['infrastructure']['residential_type'] = [];
        foreach(Expression::where('type', 'residential_type')->get() as $type)
            $data['infrastructure']['residential_type'][$type->const_id] = $type->text;

        $data['infrastructure']['house_class'] = [];
        foreach(Expression::where('type', 'house_class')->get() as $type)
            $data['infrastructure']['house_class'][$type->const_id] = $type->text;

        $data['infrastructure']['territory'] = [];
        foreach(Expression::where('type', 'territory')->get() as $type)
            $data['infrastructure']['territory'][$type->const_id] = $type->text;

        $data['infrastructure']['communal_payments'] = [];
        foreach(Expression::where('type', 'communal_payments')->get() as $type)
            $data['infrastructure']['communal_payments'][$type->const_id] = $type->text;

        $data['infrastructure']['gas_types'] = [];
        foreach(Expression::where('type', 'gas_types')->get() as $type)
            $data['infrastructure']['gas_types'][$type->const_id] = $type->text;

        $data['infrastructure']['heating_type'] = [];
        foreach(Expression::where('type', 'heating_type')->get() as $type)
            $data['infrastructure']['heating_type'][$type->const_id] = $type->text;

        $data['infrastructure']['sewerage_types'] = [];
        foreach(Expression::where('type', 'sewerage_types')->get() as $type)
            $data['infrastructure']['sewerage_types'][$type->const_id] = $type->text;

        $data['infrastructure']['water_types'] = [];
        foreach(Expression::where('type', 'water_types')->get() as $type)
            $data['infrastructure']['water_types'][$type->const_id] = $type->text;

        $data['reg_pay']['mortage'][1] = "Нет";
        $data['reg_pay']['mortage'][10] = "Да";

        $data['reg_pay']['flat_type'] = [];
        foreach(Expression::where('type', 'flat_type')->get() as $expression)
            $data['reg_pay']['flat_type'][$expression->const_id] = $expression->text;

        $data['reg_pay']['sum_contract'][1] = "Полная";
        $data['reg_pay']['sum_contract'][10] = "Не полная";

        return $data;
    }

    /**
     * @OA\Get(path="/residential/list",
     *   tags={"Жилой комплекс"},
     *   operationId="listResidentials",
     *   summary="Получить все ЖК",
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  {
     *                      "id": 1,
     *                      "builder_id": 1,
     *                      "coordinates": "40.5333, 53.220",
     *                      "name": "Residential",
     *                      "desc": "Cool",
     *                      "infrastructure": {
     *                          "status": 1,
     *                          "house_type": null,
     *                          "house_class": 2,
     *                          "technology_build": "Rock and Panel",
     *                          "territory": 1,
     *                          "distance_sea": 1000,
     *                          "communal": 1,
     *                          "height_ceiling": "3.40",
     *                          "gas": 1,
     *                          "heating": 2,
     *                          "sewerage": 1,
     *                          "water": 1
     *                      },
     *                      "reg_pay": {
     *                          "registration": "Simply",
     *                          "mortage": 1,
     *                          "flat_type": 2,
     *                          "sum_contract": 10
     *                      },
     *                      "center": {
     *                          "firstname": "Vika",
     *                          "lastname": "Blade",
     *                          "phone": "0987836898",
     *                          "email": "vika@gmail.com"
     *                      }
     *                  },
     *                  {
     *                      "id": 2,
     *                      "builder_id": 1,
     *                      "name": "Residential",
     *                      "desc": "Cool",
     *                      "infrastructure": {
     *                          "status": 1,
     *                          "house_type": null,
     *                          "house_class": 2,
     *                          "technology_build": "Rock and Panel",
     *                          "territory": 1,
     *                          "distance_sea": 1000,
     *                          "communal": 1,
     *                          "height_ceiling": "3.40",
     *                          "gas": 1,
     *                          "heating": 2,
     *                          "sewerage": 1,
     *                          "water": 1
     *                      },
     *                      "reg_pay": {
     *                          "registration": "Simply",
     *                          "mortage": 1,
     *                          "flat_type": 2,
     *                          "sum_contract": 10
     *                      },
     *                      "center": {
     *                          "firstname": "Vika",
     *                          "lastname": "Blade",
     *                          "phone": "0987836898",
     *                          "email": "vika@gmail.com"
     *                      }
     *                  }
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function listResidentials()
    {
        return response()->json(ResidentialResource::collection(Residential::get()));
    }

    /**
     * @OA\Get(path="/residential/builder/get",
     *   tags={"Жилой комплекс"},
     *   operationId="getBuilderResidentials",
     *   summary="Получить все ЖК авторизованного застройщика",
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  {
     *                      "id": 1,
     *                      "builder_id": 1,
     *                      "coordinates": "40.5333, 53.220",
     *                      "name": "Residential",
     *                      "desc": "Cool",
     *                      "infrastructure": {
     *                          "status": 1,
     *                          "house_type": null,
     *                          "house_class": 2,
     *                          "technology_build": "Rock and Panel",
     *                          "territory": 1,
     *                          "distance_sea": 1000,
     *                          "communal": 1,
     *                          "height_ceiling": "3.40",
     *                          "gas": 1,
     *                          "heating": 2,
     *                          "sewerage": 1,
     *                          "water": 1
     *                      },
     *                      "reg_pay": {
     *                          "registration": "Simply",
     *                          "mortage": 1,
     *                          "flat_type": 2,
     *                          "sum_contract": 10
     *                      },
     *                      "center": {
     *                          "firstname": "Vika",
     *                          "lastname": "Blade",
     *                          "phone": "0987836898",
     *                          "email": "vika@gmail.com"
     *                      }
     *                  },
     *                  {
     *                      "id": 2,
     *                      "builder_id": 1,
     *                      "name": "Residential",
     *                      "desc": "Cool",
     *                      "infrastructure": {
     *                          "status": 1,
     *                          "house_type": null,
     *                          "house_class": 2,
     *                          "technology_build": "Rock and Panel",
     *                          "territory": 1,
     *                          "distance_sea": 1000,
     *                          "communal": 1,
     *                          "height_ceiling": "3.40",
     *                          "gas": 1,
     *                          "heating": 2,
     *                          "sewerage": 1,
     *                          "water": 1
     *                      },
     *                      "reg_pay": {
     *                          "registration": "Simply",
     *                          "mortage": 1,
     *                          "flat_type": 2,
     *                          "sum_contract": 10
     *                      },
     *                      "center": {
     *                          "firstname": "Vika",
     *                          "lastname": "Blade",
     *                          "phone": "0987836898",
     *                          "email": "vika@gmail.com"
     *                      }
     *                  }
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function getBuilderResidentials()
    {
        return response()->json(ResidentialResource::collection(Residential::where('builder_id', Auth::user()->id)->get()));
    }

    /**
     * @OA\Get(path="/residential/form/create",
     *   tags={"Жилой комплекс"},
     *   operationId="createFormResidential",
     *   summary="Форма создания ЖК",
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "infrastructure": {
     *                      "residential_status": {
     *                          "1": "Квартиры",
     *                          "2": "Апартаменты"
     *                      },
     *                      "residential_type": {
     *                          "1": "Многоквартирный",
     *                          "2": "Апартаментный"
     *                      },
     *                      "house_class": {
     *                          "1": "Элитный",
     *                          "2": "Бюджетный"
     *                      },
     *                      "territory": {
     *                          "1": "Закрытая охраняемая",
     *                          "2": "Нет"
     *                      },
     *                      "communal_payments": {
     *                          "1": "Платежи"
     *                      },
     *                      "gas_types": {
     *                          "1": "Центральный",
     *                          "2": "Нет"
     *                      },
     *                      "heating_type": {
     *                          "1": "Центральное",
     *                          "2": "Газовое",
     *                          "3": "Нет"
     *                      },
     *                      "sewerage_types": {
     *                          "1": "Центральное",
     *                          "2": "Нет"
     *                      },
     *                      "water_types": {
     *                          "1": "Центральное",
     *                          "2": "Нет"
     *                      }
     *                  },
     *                  "reg_pay": {
     *                      "mortage": {
     *                          "1": "Нет",
     *                          "10": "Да"
     *                      },
     *                      "flat_type": {
     *                          "1": "Апартаменты",
     *                          "2": "Квартира",
     *                          "3": "Кладовая"
     *                      },
     *                      "sum_contract": {
     *                          "1": "Полная",
     *                          "10": "Не полная"
     *                      }
     *                  }
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function createFormResidential()
    {
        return response()->json($this->getFormParameters());
    }

    /**
     * @OA\Get(path="/residential/form/update",
     *   tags={"Жилой комплекс"},
     *   operationId="updateFormResidential",
     *   summary="Форма редактирования ЖК",
     *     @OA\RequestBody(
     *     description="",
     *              @OA\JsonContent(
     *             @OA\Property(property="residential_id", type="integer", example="1"),
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                 "residential": {
     *                      "id": 1,
     *                      "builder_id": 1,
     *                      "coordinates": "40.5333, 53.220",
     *                      "name": "ЖК Колесный",
     *                      "desc": "ЖК Колесный",
     *                      "infrastructure": {
     *                          "status": 1,
     *                          "house_type": null,
     *                          "house_class": 1,
     *                          "technology_build": "1",
     *                          "territory": 1,
     *                          "distance_sea": 120,
     *                          "communal": 1,
     *                          "height_ceiling": "3.00",
     *                          "gas": 1,
     *                          "heating": 1,
     *                          "sewerage": 1,
     *                          "water": 1
     *                      },
     *                      "reg_pay": {
     *                          "registration": "1",
     *                          "mortage": 1,
     *                          "flat_type": 1,
     *                          "sum_contract": 10000
     *                      },
     *                      "center": {
     *                          "firstname": "Женя",
     *                          "lastname": "Березовская",
     *                          "phone": "065012235",
     *                          "email": "097522586"
     *                      }
     *                  },
     *                  "data": {
     *                      "infrastructure": {
     *                          "residential_status": {
     *                              "1": "Квартиры",
     *                              "2": "Апартаменты"
     *                          },
     *                          "residential_type": {
     *                              "1": "Многоквартирный",
     *                              "2": "Апартаментный"
     *                          },
     *                          "house_class": {
     *                              "1": "Элитный",
     *                              "2": "Бюджетный"
     *                          },
     *                          "territory": {
     *                              "1": "Закрытая охраняемая",
     *                              "2": "Нет"
     *                          },
     *                          "communal_payments": {
     *                              "1": "Платежи"
     *                          },
     *                          "gas_types": {
     *                              "1": "Центральный",
     *                              "2": "Нет"
     *                          },
     *                          "heating_type": {
     *                              "1": "Центральное",
     *                              "2": "Газовое",
     *                              "3": "Нет"
     *                          },
     *                          "sewerage_types": {
     *                              "1": "Центральное",
     *                              "2": "Нет"
     *                          },
     *                          "water_types": {
     *                              "1": "Центральное",
     *                              "2": "Нет"
     *                          }
     *                      },
     *                      "reg_pay": {
     *                          "mortage": {
     *                              "1": "Нет",
     *                              "10": "Да"
     *                          },
     *                          "flat_type": {
     *                              "1": "Апартаменты",
     *                              "2": "Квартира",
     *                              "3": "Кладовая"
     *                          },
     *                          "sum_contract": {
     *                              "1": "Полная",
     *                              "10": "Не полная"
     *                          }
     *                      }
     *                  }
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function updateFormResidential(Request $request)
    {
        return response()->json(['residential' => new ResidentialResource(Residential::where('id', $request->residential_id)->first()) ,'data' => $this->getFormParameters()]);
    }

    /**
     * @OA\Get(path="/residential/advantages/get",
     *   tags={"Жилой комплекс"},
     *   operationId="getAdvantages",
     *   summary="Получить преимущества ЖК",
     *     @OA\RequestBody(
     *     description="",
     *              @OA\JsonContent(
     *             @OA\Property(property="residential_id", type="integer", example="1"),
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "active_advantages": {
     *                      {
     *                      "id": 1,
     *                      "name": "Спортивная площадка"
     *                      }
     *                   },
     *                  "advantages": {
     *                      {
     *                      "id": 1,
     *                      "name": "Спортивная площадка"
     *                      },
     *                      {
     *                      "id": 2,
     *                      "name": "Парковка"
     *                      }
     *                  }
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function getFormAdvantages(Request $request)
    {
        if (Residential::where('id', $request->residential_id)->first() == null)
            return response()->json(['status' => 200, 'message' => 'Residential not found']);

        return response()->json([
            'active_advantages' => collect(ResidentialAdvantage::selectRaw('advantage_id')->where('residential_id', $request->residential_id)->get()),
            'advantages' => Advantage::get()
        ]);
    }

    /**
     * @OA\Post(path="/residential/create",
     *   tags={"Жилой комплекс"},
     *   operationId="createResidential",
     *   summary="Создать новый ЖК",
     *     @OA\RequestBody(
     *     description="",
     *              @OA\JsonContent(
     *             @OA\Property(property="coordinates", type="string", example="40.5333, 53.220"),
     *             @OA\Property(property="name", type="string", example="Residential"),
     *             @OA\Property(property="desc", type="string", example="Cool"),
     *             @OA\Property(property="redidential_status", type="integer", example="1"),
     *             @OA\Property(property="redidential_type", type="integer", example="1"),
     *             @OA\Property(property="house_class", type="integer", example="2"),
     *             @OA\Property(property="technology_build", type="string", example="Rock and Panel"),
     *             @OA\Property(property="territory", type="integer", example="1"),
     *             @OA\Property(property="distance_sea", type="integer", example="1000"),
     *             @OA\Property(property="communal_payments", type="integer", example="1"),
     *             @OA\Property(property="height_ceiling", type="float", example="3.4"),
     *             @OA\Property(property="gas_types", type="integer", example="1"),
     *             @OA\Property(property="heating_type", type="integer", example="2"),
     *             @OA\Property(property="sewerage_types", type="integer", example="1"),
     *             @OA\Property(property="water_types", type="integer", example="1"),
     *             @OA\Property(property="registration", type="string", example="Simply"),
     *             @OA\Property(property="mortage", type="integer", example="1"),
     *             @OA\Property(property="flat_type", type="integer", example="2"),
     *             @OA\Property(property="sum_contract", type="integer", example="10"),
     *             @OA\Property(property="center_firstname", type="string", example="Vika"),
     *             @OA\Property(property="center_lastname", type="string", example="Blade"),
     *             @OA\Property(property="center_phone", type="string", example="0987836898"),
     *             @OA\Property(property="center_email", type="string", example="vika@gmail.com"),
     *             @OA\Property(property="excels", type="object", example={"name": "file"}),
     *             @OA\Property(property="pdf", type="object", example={"name": "file"}),
     *             @OA\Property(property="photos", type="object", example={"\position (1)\": "file"})
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 200,
     *                  "message": "Residential created",
     *                  "residential_id": 1
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function createResidential(Request $request)
    {
        return response()->json(['status' => 200, 'message' => 'Residential created', 'residential_id' => Residential::createResidential($request)]);
    }

    /**
     * @OA\Post(path="/residential/update",
     *   tags={"Жилой комплекс"},
     *   operationId="updateResidential",
     *   summary="Обновить ЖК",
     *     @OA\RequestBody(
     *     description="",
     *              @OA\JsonContent(
     *             @OA\Property(property="residential_id", type="integer", example="1"),
     *             @OA\Property(property="coordinates", type="string", example="40.5333, 53.220"),
     *             @OA\Property(property="name", type="string", example="Residential"),
     *             @OA\Property(property="desc", type="string", example="Cool"),
     *             @OA\Property(property="redidential_status", type="integer", example="1"),
     *             @OA\Property(property="redidential_type", type="integer", example="1"),
     *             @OA\Property(property="house_class", type="integer", example="2"),
     *             @OA\Property(property="technology_build", type="string", example="Rock and Panel"),
     *             @OA\Property(property="territory", type="integer", example="1"),
     *             @OA\Property(property="distance_sea", type="integer", example="1000"),
     *             @OA\Property(property="communal_payments", type="integer", example="1"),
     *             @OA\Property(property="height_ceiling", type="float", example="3.4"),
     *             @OA\Property(property="gas_types", type="integer", example="1"),
     *             @OA\Property(property="heating_type", type="integer", example="2"),
     *             @OA\Property(property="sewerage_types", type="integer", example="1"),
     *             @OA\Property(property="water_types", type="integer", example="1"),
     *             @OA\Property(property="registration", type="string", example="Simply"),
     *             @OA\Property(property="mortage", type="integer", example="1"),
     *             @OA\Property(property="flat_type", type="integer", example="2"),
     *             @OA\Property(property="sum_contract", type="integer", example="10"),
     *             @OA\Property(property="center_firstname", type="string", example="Vika"),
     *             @OA\Property(property="center_lastname", type="string", example="Blade"),
     *             @OA\Property(property="center_phone", type="string", example="0987836898"),
     *             @OA\Property(property="center_email", type="string", example="vika@gmail.com"),
     *             @OA\Property(property="excels", type="object", example={"name": "file"}),
     *             @OA\Property(property="pdf", type="object", example={"name": "file"}),
     *             @OA\Property(property="photos", type="object", example={"\position (1)\": "file"}),
     *             @OA\Property(property="delete_documents", type="object", example={"0": "\document_id (1)\"}),
     *             @OA\Property(property="delete_photos", type="object", example={"0": "\position (1)\"}),
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 200,
     *                  "message": "Residential updated"
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function updateResidential(Request $request)
    {
        $residential = Residential::where('id', $request->residential_id)->first();
        if ($residential == null || $residential->builder_id != Auth::user()->id)
            return response()->json(['status' => 300, 'message' => 'Residential access denied']);

        Residential::updateResidential($request, $request->residential_id);
        return response()->json(['status' => 200, 'message' => 'Residential updated']);
    }

    /**
     * @OA\Post(path="/residential/advantage/set",
     *   tags={"Жилой комплекс"},
     *   operationId="setAdvantages",
     *   summary="Установить преимущества на ЖК",
     *     @OA\RequestBody(
     *     description="",
     *              @OA\JsonContent(
     *             @OA\Property(property="residential_id", type="int", example="1"),
     *             @OA\Property(property="active_advantages", type="object", example={"0": "2", "1": "3", "2": "5"}),
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 200,
     *                  "message": "Advantages set"
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function setAdvantages(Request $request)
    {
        Advantage::setAdvantages($request, $request->residential_id);
        return response()->json(['status' => 200, 'message' => 'Advantages set']);
    }

    /**
     * @OA\Post(path="/residential/delete",
     *   tags={"Жилой комплекс"},
     *   operationId="deleteResidential",
     *   summary="Удалить ЖК",
     *     @OA\RequestBody(
     *     description="",
     *              @OA\JsonContent(
     *             @OA\Property(property="residential_id", type="int", example="1"),
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 200,
     *                  "message": "Residential deleted"
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function deleteResidential(Request $request)
    {
        Residential::deleteResidential($request->residential_id);
        return response()->json(['status' => 200, 'message' => 'Residential deleted']);
    }

}
