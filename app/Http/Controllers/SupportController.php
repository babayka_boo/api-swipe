<?php

namespace App\Http\Controllers;

use App\Http\Resources\ChatResource;
use App\Models\Center;
use App\Models\Support;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Events\Chat;

class SupportController extends Controller
{
    /**
     * @OA\Get(path="/support/centers/get",
     *   tags={"Поддержка"},
     *   operationId="getMFC",
     *   summary="Получить МФЦ",
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  {
     *                      "id": 1,
     *                      "name": "Наш центр МФС",
     *                      "coordinates": "46.401720686551094, 30.712291698010795"
     *                  }
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function getMFC()
    {
        return response()->json(collect(Center::get()));
    }

    /**
     * @OA\Get(path="/support/chat/{admin\user}/get",
     *   tags={"Поддержка"},
     *   operationId="getAdminChat",
     *   summary="Получить сообщения чата",
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  {
     *                      "id": 1,
     *                      "type": "to",
     *                      "message": "Hello, I have a problem",
     *                      "created_at": "2022-03-23 16:15"
     *                  },
     *                  {
     *                      "id": 2,
     *                      "type": "from",
     *                      "message": "Hello, I'm helper",
     *                      "created_at": "2022-03-23 16:15"
     *                  }
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function getChat()
    {
        return response()->json(ChatResource::collection(Support::where('user_id', Auth::user()->id)->orderBy('created_at')->get()));
    }

    /**
     * @OA\Post(path="/support/chat/user/send",
     *   tags={"Поддержка"},
     *   operationId="sendUserMessage",
     *   summary="Отправить сообщение в чат -пользователь",
     *   @OA\RequestBody(
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Hello, I have a problem"),
     *    ),
     *   ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  {
     *                     "status": 200,
     *                     "message": "Message sent"
     *                  }
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function sendUserMessage(Request $request)
    {
        Support::insert([
            'type' => 'to',
            'user_id' => Auth::user()->id,
            'message' => $request->message,
        ]);
        $message = Support::where('id', Support::max('id'))->first();
        event(
            new Chat($message)
        );

        return response()->json(['status' => 200, 'message' => 'Message sent']);
    }

    /**
     * @OA\Post(path="/support/chat/admin/send",
     *   tags={"Поддержка"},
     *   operationId="sendAdminMessage",
     *   summary="Отправить сообщение в чат -админ",
     *   @OA\RequestBody(
     *    @OA\JsonContent(
     *       @OA\Property(property="user_id", type="integer", example="1"),
     *       @OA\Property(property="message", type="string", example="Hello, I have a solve problem"),
     *    ),
     *   ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  {
     *                     "status": 200,
     *                     "message": "Message sent"
     *                  }
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function sendAdminMessage(Request $request)
    {
        Support::insert([
            'type' => 'from',
            'user_id' => $request->user_id,
            'message' => $request->message,
        ]);
        return response()->json(['status' => 200, 'message' => 'Message sent']);
    }
}
