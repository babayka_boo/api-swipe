<?php

namespace App\Http\Controllers;

use App\Http\Resources\GridRequestResource;
use App\Http\Resources\PostResource;
use App\Models\Grid;
use App\Models\GridRequest;
use App\Models\Post;
use App\Models\Residential;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GridController extends Controller
{
    /**
     * @OA\Get(path="/residential/grid/get",
     *   tags={"Шахматка"},
     *   operationId="getGrid",
     *   summary="Получить шахматку ЖК",
     *     @OA\RequestBody(
     *     description="Секций у всех ЖК - 8, столбцов и колонок - 8, значение колонки - post_id",
     *              @OA\JsonContent(
     *             @OA\Property(property="residential_id", type="integer", example="1"),
     *             @OA\Property(property="section", type="integer", example="1"),
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "1": {
     *                      "1": null,
     *                      "2": null,
     *                      "3": null,
     *                      "4": null,
     *                      "5": null,
     *                      "6": null,
     *                      "7": null,
     *                      "8": null
     *                  },
     *                  "2": {
     *                      "1": 1,
     *                      "2": null,
     *                      "3": null,
     *                      "4": null,
     *                      "5": null,
     *                      "6": null,
     *                      "7": null,
     *                      "8": null
     *                  },
     *                  "3": {
     *                      "1": null,
     *                      "2": null,
     *                      "3": null,
     *                      "4": null,
     *                      "5": null,
     *                      "6": null,
     *                      "7": null,
     *                      "8": null
     *                  },
     *                  "4": {
     *                      "1": null,
     *                      "2": null,
     *                      "3": null,
     *                      "4": null,
     *                      "5": null,
     *                      "6": null,
     *                      "7": null,
     *                      "8": null
     *                  },
     *                  "5": {
     *                      "1": null,
     *                      "2": null,
     *                      "3": null,
     *                      "4": null,
     *                      "5": null,
     *                      "6": null,
     *                      "7": null,
     *                      "8": null
     *                  },
     *                  "6": {
     *                      "1": null,
     *                      "2": null,
     *                      "3": null,
     *                      "4": null,
     *                      "5": null,
     *                      "6": null,
     *                      "7": null,
     *                      "8": null
     *                  },
     *                  "7": {
     *                      "1": null,
     *                      "2": null,
     *                      "3": null,
     *                      "4": null,
     *                      "5": null,
     *                      "6": null,
     *                      "7": null,
     *                      "8": null
     *                  },
     *                  "8": {
     *                      "1": null,
     *                      "2": null,
     *                      "3": null,
     *                      "4": null,
     *                      "5": null,
     *                      "6": null,
     *                      "7": null,
     *                      "8": null
     *                  }
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function getGrid(Request $request)
    {
        $grid = [];
        for ($i = 1; $i < 9; $i++)
        {
            for ($j = 1; $j < 9; $j++)
            {
                $temp_grid = Grid::where('residential_id', $request->residential_id)
                    ->where('section', $request->section)
                    ->where('row', $i)
                    ->where('col', $j)
                    ->first();
                $grid[$i][$j] = $temp_grid != null ? $temp_grid->post_id : null;
            }
        }
        return response()->json($grid);
    }

    /**
     * @OA\Get(path="/residential/grid/request/get",
     *   tags={"Шахматка"},
     *   operationId="getGridRequests",
     *   summary="Получить запросы на вступление в шахматку",
     *     @OA\RequestBody(
     *     description="",
     *              @OA\JsonContent(
     *             @OA\Property(property="residential_id", type="integer", example="1")
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  {
     *                      "id": 1,
     *                      "post": {
     *                          "id": 2,
     *                          "address": "ул. Темерязева 7",
     *                          "address_coordinate": "50.483723727569846, 30.490088493285988",
     *                          "residential_id": 1,
     *                          "document_owner": 1,
     *                          "flat_type": 2,
     *                          "count_room": 1,
     *                          "flat_state": 1,
     *                          "plans": {
     *                              {
     *                                  "id": 1,
     *                                  "name": "Студия"
     *                              },
     *                              {
     *                                  "id": 2,
     *                                  "name": "Сан. узел"
     *                              }
     *                          },
     *                          "square": "72.00",
     *                          "square_kitchen": "20.00",
     *                          "balcony": 1,
     *                          "heating_type": 3,
     *                          "commission": 100000,
     *                          "mortage": 1,
     *                          "connection_type": 1,
     *                          "desc": "Flat is norm",
     *                          "gallery": {},
     *                          "price": 34000000,
     *                          "payment_type": 1,
     *                          "post_type_id": 1,
     *                          "views": 0,
     *                          "moderate_accept": 1,
     *                          "promotions": {}
     *                      }
     *                  }
     *              }
     *          ),
     *        }
     *    ),
     *     @OA\Response(
     *      response="301",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 301,
     *                  "message": "Access denied"
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function getGridRequests(Request $request)
    {
        if (Residential::where('id', $request->residential_id)->first()->builder_id == Auth::user()->id)
            return response()->json(GridRequestResource::collection(GridRequest::where('residential_id', $request->residential_id)->get()));

        return response()->json(['status' => 301, 'message' => 'Access denied'], 301);
    }


    /**
     * @OA\Get(path="/residential/grid/cell/get",
     *   tags={"Шахматка"},
     *   operationId="getGridCell",
     *   summary="Получить ячейку",
     *     @OA\RequestBody(
     *     description="",
     *              @OA\JsonContent(
     *             @OA\Property(property="residential_id", type="integer", example="1"),
     *             @OA\Property(property="section", type="integer", example="1"),
     *             @OA\Property(property="row", type="integer", example="1"),
     *             @OA\Property(property="col", type="integer", example="1"),
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "id": 1,
     *                  "address": "ул. Княжеская 7",
     *                  "address_coordinate": "50.483723727569846, 30.490088493285988",
     *                  "residential_id": 1,
     *                  "document_owner": 1,
     *                  "flat_type": 2,
     *                  "count_room": 2,
     *                  "flat_state": 1,
     *                  "plans": {
     *                  {
     *                  "id": 1,
     *                  "name": "Студия"
     *                  },
     *                  {
     *                  "id": 2,
     *                  "name": "Сан. узел"
     *                  }
     *                  },
     *                  "square": "72.00",
     *                  "square_kitchen": "20.00",
     *                  "balcony": 1,
     *                  "heating_type": 3,
     *                  "commission": 100000,
     *                  "mortage": 1,
     *                  "connection_type": 1,
     *                  "desc": "Flat is good",
     *                  "gallery": {},
     *                  "price": 34000000,
     *                  "payment_type": 1,
     *                  "post_type_id": 1,
     *                  "views": 9,
     *                  "moderate_accept": 10,
     *                  "promotions": {
     *                  {
     *                  "id": 1,
     *                  "name": "Турбо",
     *                  "desc": "Поднятие вашего объявления в топ",
     *                  "image": "http://api-swipe.com/images/promotions/promotion-turbo.png",
     *                  "efficiency": 90,
     *                  "price": 499,
     *                  "type": "turbo",
     *                  "value": null
     *                  },
     *                  {
     *                  "id": 2,
     *                  "name": "Поднятие Объявления",
     *                  "desc": "Поднять объявление и закрепить",
     *                  "image": "http://api-swipe.com/images/promotions/promotion-top.png",
     *                  "efficiency": 60,
     *                  "price": 399,
     *                  "type": "color",
     *                  "value": "green"
     *                  }
     *                  }
     *              }
     *          ),
     *        }
     *    ),
     *     @OA\Response(
     *      response="300",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 300,
     *                  "message": "Cell is empty"
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function getGridCell(Request $request)
    {
        $cell = Grid::where('residential_id', $request->residential_id)
            ->where('section', $request->section)
            ->where('row', $request->row)
            ->where('col', $request->col)
            ->first();
        if ($cell == null)
            return response()->json(['status' => 300, 'message' => 'Cell is empty'], 300);

        return response()->json(new PostResource(Post::where('id', $cell->post_id)->first()));
    }

    /**
     * @OA\Post(path="/residential/grid/cell/write",
     *   tags={"Шахматка"},
     *   operationId="writeCell",
     *   summary="Записать в ячейку",
     *     @OA\RequestBody(
     *     description="",
     *              @OA\JsonContent(
     *             @OA\Property(property="residential_id", type="integer", example="1"),
     *             @OA\Property(property="section", type="integer", example="1"),
     *             @OA\Property(property="row", type="integer", example="1"),
     *             @OA\Property(property="col", type="integer", example="1"),
     *             @OA\Property(property="post_id", type="integer", example="1"),
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 200,
     *                  "message": "Cell wrote"
     *              }
     *          ),
     *        }
     *    ),
     *     @OA\Response(
     *      response="300",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 300,
     *                  "message": "Cell isn't empty"
     *              }
     *          ),
     *        }
     *    ),
     *
     *     @OA\Response(
     *      response="301",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 301,
     *                  "message": "Access denied"
     *              }
     *          ),
     *        }
     *    ),
     *     @OA\Response(
     *      response="302",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 302,
     *                  "message": "Column not exist"
     *              }
     *          ),
     *        }
     *    ),
     *     @OA\Response(
     *      response="303",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 303,
     *                  "message": "Row not exist"
     *              }
     *          ),
     *        }
     *    ),
     *     @OA\Response(
     *      response="304",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 304,
     *                  "message": "Post has already cell",
     *                  "cell": {
     *                      "residential_id": 1,
     *                      "section": 1,
     *                      "row": 1,
     *                      "col": 1
     *                  }
     *              }
     *          ),
     *        }
     *    ),
     *     @OA\Response(
     *      response="305",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 305,
     *                  "message": "Section not exist"
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function writeCell(Request $request)
    {
        if (Residential::where('id', $request->residential_id)->first()->builder_id != Auth::user()->id)
            return response()->json(['status' => 301, 'message' => 'Access denied'], 301);

        if (Grid::where('residential_id', $request->residential_id)
                ->where('section', $request->section)
                ->where('row', $request->row)
                ->where('col', $request->col)
                ->first() != null)
            return response()->json(['status' => 300, 'message' => 'Cell isn\'t empty'], 300);

        if ($request->section > 4)
            return response()->json(['status' => 305, 'message' => 'Section not exist'], 305);
        else if ($request->col > 8)
            return response()->json(['status' => 302, 'message' => 'Column not exist'], 302);
        else if ($request->row > 8)
            return response()->json(['status' => 303, 'message' => 'Row not exist'], 303);

        if (Grid::where('post_id', $request->post_id)->first() != null) {
            $cell = Grid::where('post_id', $request->post_id)
                ->first();
            return response()->json(['status' => 304,
                'message' => 'Post has already cell',
                'cell' => [
                    'residential_id' => $cell->residential_id,
                    'section' => $cell->section,
                    'row' => $cell->row,
                    'col' => $cell->col
                ]], 304);
        }

        Grid::writeCell($request);
        return response()->json(['status' => 200, 'message' => 'Cell wrote']);
    }

    /**
     * @OA\Post(path="/residential/grid/cell/clear",
     *   tags={"Шахматка"},
     *   operationId="clearCell",
     *   summary="Очистить в ячейку",
     *     @OA\RequestBody(
     *     description="",
     *              @OA\JsonContent(
     *             @OA\Property(property="residential_id", type="integer", example="1"),
     *             @OA\Property(property="section", type="integer", example="1"),
     *             @OA\Property(property="row", type="integer", example="1"),
     *             @OA\Property(property="col", type="integer", example="1"),
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 200,
     *                  "message": "Cell cleared"
     *              }
     *          ),
     *        }
     *    ),
     *     @OA\Response(
     *      response="301",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 301,
     *                  "message": "Section not exist"
     *              }
     *          ),
     *        }
     *    ),
     *     @OA\Response(
     *      response="302",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 302,
     *                  "message": "Column not exist"
     *              }
     *          ),
     *        }
     *    ),
     *     @OA\Response(
     *      response="303",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 303,
     *                  "message": "Row not exist"
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function clearCell(Request $request)
    {
        if ($request->section > 4)
            return response()->json(['status' => 301, 'message' => 'Section not exist'], 301);
        else if ($request->col > 8)
            return response()->json(['status' => 302, 'message' => 'Column not exist'], 302);
        else if ($request->row > 8)
            return response()->json(['status' => 303, 'message' => 'Row not exist'], 303);

        Grid::clearCell($request);
        return response()->json(['status' => 200, 'message' => 'Cell cleared']);
    }
}
