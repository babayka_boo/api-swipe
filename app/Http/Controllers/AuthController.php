<?php

namespace App\Http\Controllers;

use App\Mail\SendAuthToken;
use App\Models\Admin;
use App\Models\AuthToken;
use App\Models\Builder;
use App\Models\User;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class AuthController extends Controller
{

    /**
     * @OA\Post(path="/login",
     *   tags={"Авторизация"},
     *   operationId="getAuthCode",
     *   summary="Получение кода на e-mail",
     *   @OA\RequestBody(
     *    required=true,
     *    description="Phone number",
     *    @OA\JsonContent(
     *       required={"phone"},
     *       @OA\Property(property="phone", type="string", format="phone", example="0665051158"),
     *    ),
     * ),
     *  @OA\Response(
     *      response="200",
     *      description="Message sent",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 200,
     *                  "phone": "0665043348",
     *                  "message": "auth_token sent on email"
     *              },
     *          ),
     *        }
     *    ),
     *   @OA\Response(
     *      response="300",
     *      description="Phone number incorrect",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 300,
     *                  "message": "Not found phone"
     *              },
     *          ),
     *        }
     *    ),
     * )
     */

    public function login(Request $request)
    {
        $email = '';
        if(User::where('phone', $request->phone)->first() != null)
            $email = User::where('phone', $request->phone)->first()->email;
        elseif(Admin::where('phone', $request->phone)->first() != null)
            $email = Admin::where('phone', $request->phone)->first()->email;
        elseif(Builder::where('phone', $request->phone)->first() != null)
            $email = Builder::where('phone', $request->phone)->first()->email;
        else
            return response()->json(['status' => 300,'message' => 'Not found phone'], 300);


        $token = random_int(0,9).random_int(0,9).random_int(0,9).random_int(0,9);
        Mail::to($email)->send(new SendAuthToken($token));
        AuthToken::insert([
            'phone' => $request->phone,
            'auth_token' => $token
        ]);
        return response()->json(['status' => 200,'phone' => $request->phone, 'message' => 'auth_token sent on email']);
    }

    /**
     * @OA\Post(path="/login/confirm",
     *   tags={"Авторизация"},
     *   operationId="ConfirmCode",
     *   summary="Подтверждение кода с email",
     *   @OA\RequestBody(
     *    required=true,
     *    description="Phone number and code",
     *    @OA\JsonContent(
     *       required={"phone", "code"},
     *       @OA\Property(property="phone", type="string", format="phone", example="0665051158"),
     *       @OA\Property(property="code", type="int", example="4252"),
     *    ),
     * ),
     *     @OA\Response(
     *      response="200",
     *      description="Authorized",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 200,
     *                  "token": "4325dsd2486464d4s53431",
     *                  "message": "User authorized"
     *              },
     *          ),
     *        }
     *    ),
     *
     *     @OA\Response(
     *      response="401",
     *      description="Code timeout",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 401,
     *                  "message": "Code Timeout"
     *              },
     *          ),
     *        }
     *    ),
     *     @OA\Response(
     *      response="301",
     *      description="Incorrect code",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 301,
     *                  "message": "Incorrect code"
     *              },
     *          ),
     *        }
     *    ),
     * )
     */

    public function confirmCode(Request $request)
    {
        $date = new DateTime();
        AuthToken::where('created_at', '<', $date->modify('-5 minutes')->format('Y-m-d H:i:s'))->delete();
        if (AuthToken::orderBy('created_at', 'desc')
                ->where('phone', $request->phone)
                ->first() == null)
            return response()->json(['status' => 401,'message' => 'Code Timeout'], 401);
        elseif (AuthToken::orderBy('created_at', 'desc')
                ->where('phone', $request->phone)
                ->where('auth_token', $request->auth_code)
                ->first() == null)
            return response()->json(['status' => 301, 'message' => 'Incorrect code'], 301);

        $token = Str::random(60);
        if (User::where('phone', $request->phone)->first() != null)
        {
            User::where('phone', $request->phone)->update([
               'api_token' =>  hash('sha256', $token)
            ]);
            return response()->json(['token' => $token,'message' => 'User authorized']);
        }
        if (Admin::where('phone', $request->phone)->first() != null)
        {
            Admin::where('phone', $request->phone)->update([
                'api_token' =>  hash('sha256', $token)
            ]);
            return response()->json(['token' => $token,'message' => 'Admin authorized']);
        }
        if (Builder::where('phone', $request->phone)->first() != null)
        {
            Builder::where('phone', $request->phone)->update([
                'api_token' =>  hash('sha256', $token)
            ]);
            return response()->json(['token' => $token,'message' => 'Builder authorized']);
        }


        return response()->json(['message' => 'User not found'], 404);
    }
}
