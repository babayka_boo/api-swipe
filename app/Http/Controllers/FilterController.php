<?php

namespace App\Http\Controllers;

use App\Models\Expression;
use App\Models\Filter;
use App\Models\PostType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FilterController extends Controller
{
    /**
     * @OA\Get(path="/filter/types/get",
     *   tags={"Мои фильтры"},
     *   operationId="getFilterTypes",
     *   summary="Получить виды фильтров",
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                   {
     *                       "id": 1,
     *                       "name": "Вторичный рынок"
     *                   },
     *                   {
     *                       "id": 2,
     *                       "name": "Новостройки"
     *                   },
     *                   {
     *                       "id": 3,
     *                       "name": "Все"
     *                   },
     *                   {
     *                       "id": 4,
     *                       "name": "Коттеджы"
     *                   }
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function getFilterTypes()
    {
        return response()->json(collect(PostType::get()));
    }

    /**
     * @OA\Get(path="/filter/get",
     *   tags={"Мои фильтры"},
     *   operationId="getFilter",
     *   summary="Получить фильтр",
     *     @OA\RequestBody(
     *     description="",
     *              @OA\JsonContent(
     *             @OA\Property(property="post_type_id", type="integer", example="1"),
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "filter": {
     *                      "district": null,
     *                      "micro_district": null,
     *                      "count_room": 2,
     *                      "price_from": 0,
     *                      "price_to": 4000000,
     *                      "square_from": "50",
     *                      "square_to": null,
     *                      "flat_type": 1,
     *                      "mortage": null,
     *                      "flat_state": 1,
     *                      "save_filter": 1
     *                  },
     *                  "data": {
     *                      "districts": {
     *                          "1": "Приморский",
     *                          "2": "Киевский",
     *                          "3": "Малиновский"
     *                      },
     *                      "micro_districts": {
     *                          "1": "Центр",
     *                          "2": "Парк",
     *                          "3": "Дорожный"
     *                      },
     *                      "flat_type": {
     *                          "1": "Апартаменты",
     *                          "2": "Квартира",
     *                          "3": "Кладовая"
     *                      },
     *                      "mortage": {
     *                          "1": "Нет",
     *                          "10": "Да"
     *                      },
     *                      "flat_state": {
     *                          "1": "Жилое",
     *                          "2": "Требует ремонта",
     *                          "3": "Аварийное",
     *                          "4": "Черновая"
     *                      }
     *                  }
     *              }
     *          ),
     *        }
     *    ),
     *   @OA\Response(
     *      response="202",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 202,
     *                  "message": "Filter not set",
     *                  "data": {
     *                      "flat_type": {
     *                          "1": "Апартаменты",
     *                          "2": "Квартира",
     *                          "3": "Кладовая"
     *                      },
     *                      "mortage": {
     *                          "1": "Нет",
     *                          "10": "Да"
     *                      },
     *                      "flat_state": {
     *                          "1": "Жилое",
     *                          "2": "Требует ремонта",
     *                          "3": "Аварийное",
     *                          "4": "Черновая"
     *                      }
     *                  }
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function getFilter(Request $request)
    {
        $data['districts'] = [];
        foreach(Expression::where('type', 'districts')->get() as $expression)
            $data['districts'][$expression->const_id] = $expression->text;

        $data['micro_districts'] = [];
        foreach(Expression::where('type', 'micro_districts')->get() as $expression)
            $data['micro_districts'][$expression->const_id] = $expression->text;

        $data['flat_type'] = [];
        foreach(Expression::where('type', 'flat_type')->get() as $expression)
            $data['flat_type'][$expression->const_id] = $expression->text;

        $data['flat_state'] = [];
        foreach(Expression::where('type', 'flat_state')->get() as $expression)
            $data['flat_state'][$expression->const_id] = $expression->text;
        $data['mortage'] = [
            '1' => 'Нет',
            '10' => 'Да'
        ];


        $filter = Filter::where('user_id', Auth::user()->id)->where('post_type_id', $request->post_type_id)->first();
        if ($filter != null)
            return response()->json(['filter' => collect($filter), 'data' => $data]);


        return response()->json(['status' => 202,'message' => 'Filter not set', 'data' => $data], 202);
    }

    /**
     * @OA\Post(path="/filter/set",
     *   tags={"Мои фильтры"},
     *   operationId="setFilter",
     *   summary="Установить фильтр",
     *     @OA\RequestBody(
     *     description="",
     *              @OA\JsonContent(
     *             @OA\Property(property="post_type_id", type="integer", example="1"),
     *             @OA\Property(property="district", type="string", example="1"),
     *             @OA\Property(property="micro_district", type="string", example="2"),
     *             @OA\Property(property="count_room", type="integer", example="2"),
     *             @OA\Property(property="price_from", type="integer", example="0"),
     *             @OA\Property(property="price_to", type="integer", example="4000000"),
     *             @OA\Property(property="square_from", type="integer", example="50"),
     *             @OA\Property(property="square_to", type="integer", example="70"),
     *             @OA\Property(property="flat_type", type="integer", example="1"),
     *             @OA\Property(property="mortage", type="integer", example="10"),
     *             @OA\Property(property="flat_state", type="integer", example="1"),
     *             @OA\Property(property="save_filter", type="integer", example="1")
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 200,
     *                  "message": "Filter set"
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function setFilter(Request $request)
    {
        Filter::setFilter($request);
        return response()->json(['status' => 200,'message' => 'Filter set']);
    }
}
