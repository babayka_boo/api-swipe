<?php

namespace App\Http\Controllers;

use App\Http\Resources\PromotionResource;
use App\Models\Expression;
use App\Models\Post;
use App\Models\Promotion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PromotionController extends Controller
{
    /**
     * @OA\Get(path="/promotion/get",
     *   tags={"Продвижение"},
     *   operationId="getPromotions",
     *   summary="Получить все типы продвижения",
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  {
     *                      "id": 1,
     *                      "name": "Турбо",
     *                      "desc": "Поднятие вашего объявления в топ",
     *                      "image": "http://api-swipe.com/images/promotions/promotion-turbo.png",
     *                      "efficiency": 90,
     *                      "price": 499
     *                  },
     *                  {
     *                      "id": 2,
     *                      "name": "Поднятие Объявления",
     *                      "desc": "Поднять объявление и закрепить",
     *                      "image": "http://api-swipe.com/images/promotions/promotion-top.png",
     *                      "efficiency": 60,
     *                      "price": 399
     *                  }
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function getPromotionTypes()
    {

        return response()->json(PromotionResource::collection(Promotion::get()));
    }

    /**
     * @OA\Get(path="/promotion/phrases/get",
     *   tags={"Продвижение"},
     *   operationId="getPromotionPhrases",
     *   summary="Получить все фразы для продвижения",
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                      "1": "Подарок при покупке",
     *                      "2": "Возможен торг",
     *                      "3": "Квартира у моря",
     *                      "4": "В спальном районе",
     *                      "5": "Вам повезло с ценой",
     *                      "6": "Для большой семьи",
     *                      "7": "Семейное гнездышко",
     *                      "8": "Отдельная парковка"
     *                  }
     *          ),
     *        }
     *    ),
     * )
     */
    public function getPromotionPhrases()
    {
        $phrases = [];
        foreach(Expression::where('type','promotion_phrase')->get() as $phrase)
            $phrases[$phrase->const_id] = $phrase->text;

        return response()->json($phrases);
    }

    /**
     * @OA\Post(path="/promotion/set",
     *   tags={"Продвижение"},
     *   operationId="setPromotion",
     *   summary="Установить новые продвижения",
     *     @OA\RequestBody(
     *     description="",
     *              @OA\JsonContent(
     *             @OA\Property(property="post_id", type="integer", example="1"),
     *             @OA\Property(property="promotions", type="object", example={"1":"green", "2":""})
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 200,
     *                  "message": "Post promotions updated",
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function setPromotionsPost(Request $request)
    {
        $post = Post::where('id', $request->post_id)->first();
        if ($post != null && Auth::user()->id == $post->user_id)
        {
            Promotion::setPromotions($request);
            return response()->json(['status' => 200, 'message' => 'Post promotions updated']);
        }

        return response()->json(['status' => 301, 'message' => 'Access denied']);
    }
}
