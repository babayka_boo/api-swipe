<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @OA\Get(path="/user/list",
     *   tags={"Пользователи"},
     *   operationId="listUsers",
     *   summary="Получить всех пользователей",
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                   {
     *                      "id": 1,
     *                      "firstname": "User",
     *                      "lastname": "Usual",
     *                      "phone": "0663333333",
     *                      "email": "luckymr304@gmail.com",
     *                      "image": null
     *                  },
     *                   {
     *                      "id": 2,
     *                      "firstname": "User",
     *                      "lastname": "Usual",
     *                      "phone": "0663333333",
     *                      "email": "luckymr304@gmail.com",
     *                      "image": null
     *                  }
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function listUsers()
    {
        return response()->json(UserResource::collection(User::get()));
    }

    /**
     * @OA\Get(path="/user/list/filter",
     *   tags={"Пользователи"},
     *   operationId="listUsersByName",
     *   summary="Получить пользователей по имени",
     *      @OA\RequestBody(
     *     description="",
     *              @OA\JsonContent(
     *             @OA\Property(property="name", type="integer", example="Use")
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                   {
     *                      "id": 1,
     *                      "firstname": "User",
     *                      "lastname": "Usual",
     *                      "phone": "0663333333",
     *                      "email": "luckymr304@gmail.com",
     *                      "image": null
     *                  }
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function listUsersByName(Request $request)
    {
        $users = [];
        foreach(User::get() as $user)
            if (stripos($user->lastname . ' ' . $user->firstname, $request->name) !== FALSE || $request->name == '')
                $users[] = $user;

        return response()->json(UserResource::collection($users));
    }

    /**
     * @OA\Get(path="/user/get",
     *   tags={"Пользователи"},
     *   operationId="getUser",
     *   summary="Получить пользователя",
     *      @OA\RequestBody(
     *     description="",
     *              @OA\JsonContent(
     *             @OA\Property(property="user_id", type="integer", example="1")
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                   {
     *                      "id": 1,
     *                      "firstname": "User",
     *                      "lastname": "Usual",
     *                      "phone": "0663333333",
     *                      "email": "luckymr304@gmail.com",
     *                      "image": null
     *                  }
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function getUser(Request $request)
    {
        return response()->json(new UserResource(User::where('id', $request->user_id)->first()));
    }

    /**
     * @OA\Get(path="/user/blacklist/get",
     *   tags={"Черный список"},
     *   operationId="getBlacklist",
     *   summary="Получить пользователей из черного списка",
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                    {
     *                      "id": 1,
     *                      "firstname": "User",
     *                      "lastname": "Usual",
     *                      "phone": "0663333333",
     *                      "email": "luckymr304@gmail.com",
     *                      "image": null
     *                  }
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function getBlacklist()
    {
        return response()->json(UserResource::collection(User::where('blacklist', 10)->get()));
    }

    /**
     * @OA\Get(path="/user/blacklist/filter",
     *   tags={"Черный список"},
     *   operationId="getBlacklistByName",
     *   summary="Получить пользователей из черного списка по имени",
     *      @OA\RequestBody(
     *     description="",
     *              @OA\JsonContent(
     *             @OA\Property(property="name", type="integer", example="Use")
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                   {
     *                      "id": 1,
     *                      "firstname": "User",
     *                      "lastname": "Usual",
     *                      "phone": "0663333333",
     *                      "email": "luckymr304@gmail.com",
     *                      "image": null
     *                  }
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function getBlackListByName(Request $request)
    {
        $users = [];
        foreach (User::where('blacklist', 10)->get() as $user)
            if (stripos($user->firstname.' '.$user->lastname, $request->name) !== FALSE || $request->name == '')
                $users[] = $user;

        return response()->json(UserResource::collection($users));
    }

    /**
     * @OA\Post(path="/user/add_to_blacklist",
     *   tags={"Черный список"},
     *   operationId="addToBlacklist",
     *   summary="Добавить пользователя в черный список",
     *      @OA\RequestBody(
     *     description="",
     *              @OA\JsonContent(
     *             @OA\Property(property="user_id", type="integer", example="1")
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                   "status": 200,
     *                   "message": "User added to blacklist"
     *              }
     *          ),
     *        }
     *    ),
     *   @OA\Response(
     *      response="301",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                   "status": 301,
     *                   "message": "User not exist or has already in blacklist"
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function addToBlackList(Request $request)
    {
        $user = User::where('id', $request->user_id)->first();
        if ($user == null || $user->blacklist == 10)
            return response()->json(['status' => 301, 'message' => 'User not exist or has already in blacklist'],301);
        User::where('id', $request->user_id)->update([
           'blacklist' => 10
        ]);
        return response()->json(['status' => 200, 'message' => 'User added to blacklist']);
    }

    /**
     * @OA\Post(path="/user/remove_to_blacklist",
     *   tags={"Черный список"},
     *   operationId="removeToBlacklist",
     *   summary="Убрать пользователя из черный список",
     *      @OA\RequestBody(
     *     description="",
     *              @OA\JsonContent(
     *             @OA\Property(property="user_id", type="integer", example="1")
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                   "status": 200,
     *                   "message": "User removed to blacklist"
     *              }
     *          ),
     *        }
     *    ),
     *   @OA\Response(
     *      response="301",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                   "status": 301,
     *                   "message": "User not exist or not in blacklist"
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function removeFromBlackList(Request $request)
    {
        $user = User::where('id', $request->user_id)->first();
        if ($user == null || $user->blacklist == 1)
            return response()->json(['status' => 301, 'message' => 'User not exist or not in blacklist'],301);
        User::where('id', $request->user_id)->update([
           'blacklist' => 1
        ]);
        return response()->json(['status' => 200, 'message' => 'User removed to blacklist']);
    }


}
