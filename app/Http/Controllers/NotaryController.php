<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\Models\Notary;
use Illuminate\Http\Request;

class NotaryController extends Controller
{
    /**
     * @OA\Get(path="/notary/{user\admin}/get",
     *   tags={"Нотариус"},
     *   operationId="getNotaries",
     *   summary="Получить всех нотариусов",
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  {
     *                      "id": 1,
     *                      "firstname": "Анна",
     *                      "lastname": "Тест",
     *                      "phone": "+380952231478",
     *                      "email": "test4@gmail.com",
     *                      "image": null
     *                  },
     *                  {
     *                      "id": 2,
     *                      "firstname": "Анна",
     *                      "lastname": "Тест",
     *                      "phone": "+380952231478",
     *                      "email": "test1@gmail.com",
     *                      "image": null
     *                  },
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function getNotaries()
    {
        return response()->json(UserResource::collection(Notary::get()));
    }

    /**
     * @OA\Get(path="/notary/admin/get",
     *   tags={"Нотариус"},
     *   operationId="getNotary",
     *   summary="Получить нотариуса",
     *      @OA\RequestBody(
     *     description="",
     *              @OA\JsonContent(
     *             @OA\Property(property="notary_id", type="integer", example="1")
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "id": 2,
     *                  "firstname": "John",
     *                  "lastname": "Bonny",
     *                  "phone": "095331641",
     *                  "email": "notary@gmail.com",
     *                  "image": null
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function getNotary(Request $request)
    {
        return response()->json(new UserResource(Notary::where('id', $request->notary_id)->first()));
    }

    /**
     * @OA\Post(path="/notary/admin/add",
     *   tags={"Нотариус"},
     *   operationId="addNotary",
     *   summary="Добавить нотариуса",
     *      @OA\RequestBody(
     *     description="",
     *              @OA\JsonContent(
     *             @OA\Property(property="firstname", type="string", example="John"),
     *             @OA\Property(property="lastname", type="string", example="Bonny"),
     *             @OA\Property(property="phone", type="string", example="09531525"),
     *             @OA\Property(property="email", type="string", example="notary@gmail.com"),
     *             @OA\Property(property="image", type="string", example="file")
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                   {
     *                   "status": 200,
     *                   "message": "Notary added",
     *                   "notary_id": 3
     *                  }
     *              }
     *          ),
     *        }
     *    ),
     *   @OA\Response(
     *      response="301",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                   "status": 301,
     *                   "message": "Email already exist"
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function addNotary(Request $request)
    {
        if (Notary::where('email', $request->email)->first() != null)
            return response()->json(['status' => 301, 'message' => 'Email already exist']);

        return response()->json(['status' => 200, 'message' => 'Notary added', 'notary_id' => Notary::addNotary($request)]);
    }

    /**
     * @OA\Post(path="/notary/admin/update",
     *   tags={"Нотариус"},
     *   operationId="updateNotary",
     *   summary="Обновить нотариуса",
     *      @OA\RequestBody(
     *     description="",
     *              @OA\JsonContent(
     *             @OA\Property(property="firstname", type="string", example="John"),
     *             @OA\Property(property="lastname", type="string", example="Bonny"),
     *             @OA\Property(property="phone", type="string", example="09531525"),
     *             @OA\Property(property="email", type="string", example="notary@gmail.com"),
     *             @OA\Property(property="image", type="string", example="file")
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                   {
     *                   "status": 200,
     *                   "message": "Notary updated",
     *                   "notary_id": 3
     *                  }
     *              }
     *          ),
     *        }
     *    ),
     *   @OA\Response(
     *      response="301",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                   "status": 301,
     *                   "message": "Email already exist"
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function updateNotary(Request $request)
    {
        $notary = Notary::where('id', $request->notary_id)->first();
        if (Notary::where('email', '!=', $notary->email)->where('email', $request->email)->first() != null)
            return response()->json(['status' => 301, 'message' => 'Email already exist']);

        Notary::updateNotary($request, $request->notary_id);
        return response()->json(['status' => 200, 'message' => 'Notary updated']);
    }

    /**
     * @OA\Post(path="/notary/admin/delete",
     *   tags={"Нотариус"},
     *   operationId="deleteNotary",
     *   summary="Удалить нотариуса",
     *      @OA\RequestBody(
     *     description="",
     *              @OA\JsonContent(
     *             @OA\Property(property="notary_id", type="integer", example="1")
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                   {
     *                   "status": 200,
     *                   "message": "Notary deleted"
     *                  }
     *              }
     *          ),
     *        }
     *    ),
     *   @OA\Response(
     *      response="301",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                   "status": 301,
     *                   "message": "Notary not exist"
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function deleteNotary(Request $request)
    {
        if (Notary::where('id', $request->notary_id)->first() == null)
            return response()->json(['status' => 301, 'message' => 'Notary not exist']);

        Notary::deleteNotary($request->notary_id);
        return response()->json(['status' => 200, 'message' => 'Notary deleted']);
    }
}
