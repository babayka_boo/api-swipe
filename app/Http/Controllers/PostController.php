<?php

namespace App\Http\Controllers;

use App\Http\Resources\GalleryResource;
use App\Http\Resources\PlanResource;
use App\Http\Resources\PostResource;
use App\Models\Expression;
use App\Models\Favorite;
use App\Models\Filter;
use App\Models\Gallery;
use App\Models\Plan;
use App\Models\Post;
use App\Models\PostPlan;
use App\Models\PostPromotion;
use App\Models\PostType;
use App\Models\Residential;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    private function getFormParameters() : array
    {
        $data['districts'] = [];
        foreach(Expression::where('type', 'districts')->get() as $expression)
            $data['districts'][$expression->const_id] = $expression->text;

        $data['micro_districts'] = [];
        foreach(Expression::where('type', 'micro_districts')->get() as $expression)
            $data['micro_districts'][$expression->const_id] = $expression->text;

        $data['post_type'] = [];
        foreach(PostType::get() as $type)
            $data['post_type'][$type->id] = $type->name;

        $data['residentials'] = [];
        foreach(Residential::get() as $residential)
            $data['residentials'][$residential->id] = $residential->name;

        $data['document_owner'] = [];
        foreach(Expression::where('type', 'document_owner')->get() as $expression)
            $data['document_owner'][$expression->const_id] = $expression->text;

        $data['flat_type'] = [];
        foreach(Expression::where('type', 'flat_type')->get() as $expression)
            $data['flat_type'][$expression->const_id] = $expression->text;

        $data['plans'] = [];
        foreach(Plan::get() as $plan)
            $data['plans'][$plan->id] = $plan->name;

        $data['flat_state'] = [];
        foreach(Expression::where('type', 'flat_state')->get() as $expression)
            $data['flat_state'][$expression->const_id] = $expression->text;

        $data['balcony'][1] = "Нет";
        $data['balcony'][10] = "Да";

        $data['heating_type'] = [];
        foreach(Expression::where('type', 'heating_type')->get() as $expression)
            $data['heating_type'][$expression->const_id] = $expression->text;

        $data['payment_types'] = [];
        foreach(Expression::where('type', 'payment_types')->get() as $payment)
            $data['payment_types'][$payment->const_id] = $payment->text;

        $data['mortage'][1] = "Нет";
        $data['mortage'][10] = "Да";

        $data['connection_type'] = [];
        foreach(Expression::where('type', 'connection_type')->get() as $expression)
            $data['connection_type'][$expression->const_id] = $expression->text;
        return $data;
    }

    private function filterPosts(Builder $posts, $post_type_id) : Builder
    {
        $posts = $posts->where('post_type_id', $post_type_id);
        $filter = Filter::where('post_type_id', $post_type_id)->where('user_id', Auth::user()->id)->first();
        if (isset($filter->district))
            $posts = $posts->where('district', $filter->district);
        if (isset($filter->micro_district))
            $posts = $posts->where('micro_district', $filter->micro_district);
        if (isset($filter->count_room))
            $posts = $posts->where('count_room', $filter->count_room);
        if (isset($filter->price_from))
            $posts = $posts->where('price', '>', $filter->price_from);
        if (isset($filter->price_to))
            $posts = $posts->where('price', '<', $filter->price_to);
        if (isset($filter->square_from))
            $posts = $posts->where('square', '>', $filter->square_from);
        if (isset($filter->square_to))
            $posts = $posts->where('square', '<', $filter->square_to);
        if (isset($filter->flat_type))
            $posts = $posts->where('flat_type', $filter->flat_type);
        if (isset($filter->mortage))
            $posts = $posts->where('mortage', $filter->mortage);
        if (isset($filter->flat_state))
            $posts = $posts->where('flat_state', $filter->flat_state);
        return $posts;
    }

    /**
     * @OA\Get(path="/post/list",
     *   tags={"Мое объявление"},
     *   operationId="getPostList",
     *   summary="Получить объявления по фильтрам",
     *     @OA\RequestBody(
     *     description="",
     *          @OA\JsonContent(
     *             @OA\Property(property="post_type_id", type="integer", example="1"),
     *             @OA\Property(property="residential_id", type="integer", example="1"),
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "promotions": {
     *                      {
     *                          "id": 1,
     *                          "address": "ул. Княжеская 7",
     *                          "district": 1,
     *                          "micro_district": 2,
     *                          "residential_id": 1,
     *                          "document_owner": 1,
     *                          "flat_type": 2,
     *                          "count_room": 2,
     *                          "flat_state": 1,
     *                          "plans": {
     *                              {
     *                                  "id": 1,
     *                                  "name": "Студия"
     *                              },
     *                              {
     *                                  "id": 2,
     *                                  "name": "Сан. узел"
     *                              }
     *                          },
     *                          "square": "72.00",
     *                          "square_kitchen": "20.00",
     *                          "balcony": 1,
     *                          "heating_type": 3,
     *                          "commission": 100000,
     *                          "mortage": 1,
     *                          "connection_type": 1,
     *                          "desc": "Flat is good",
     *                          "gallery": {},
     *                          "price": 34000000,
     *                          "payment_type": 1,
     *                          "post_type_id": 1,
     *                          "views": 9,
     *                          "favorites": 0,
     *                          "moderate_accept": 10,
     *                          "promotions": {
     *                              {
     *                                  "id": 1,
     *                                  "name": "Турбо",
     *                                  "desc": "Поднятие вашего объявления в топ",
     *                                  "image": "http://api-swipe.com/images/promotions/promotion-turbo.png",
     *                                  "efficiency": 90,
     *                                  "price": 499,
     *                                  "type": "turbo",
     *                                  "value": null
     *                              }
     *                          }
     *                      }
     *                  },
     *                  "posts": {
     *                      {
     *                          "id": 2,
     *                          "address": "ул. Темерязева 7",
     *                          "district": 1,
     *                          "micro_district": 1,
     *                          "residential_id": 1,
     *                          "document_owner": 1,
     *                          "flat_type": 2,
     *                          "count_room": 1,
     *                          "flat_state": 1,
     *                          "plans": {
     *                              {
     *                                  "id": 1,
     *                                  "name": "Студия"
     *                              },
     *                              {
     *                                  "id": 2,
     *                                  "name": "Сан. узел"
     *                              }
     *                          },
     *                          "square": "72.00",
     *                          "square_kitchen": "20.00",
     *                          "balcony": 1,
     *                          "heating_type": 3,
     *                          "commission": 100000,
     *                          "mortage": 1,
     *                          "connection_type": 1,
     *                          "desc": "Flat is norm",
     *                          "gallery": {},
     *                          "price": 34000000,
     *                          "payment_type": 1,
     *                          "post_type_id": 1,
     *                          "views": 0,
     *                          "favorites": 1,
     *                          "moderate_accept": 1,
     *                          "promotions": {}
     *                      }
     *                  }
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function getPostsList(Request $request)
    {
        $promotions = PostPromotion::selectRaw('posts.*')
            ->leftJoin('posts', 'posts.id', '=', 'post_promotions.post_id')
            ->leftJoin('promotions', 'promotions.id', '=', 'post_promotions.promotion_id')
            ->where('posts.moderate_accept', 10)
            ->where('promotions.type', 'top')
            ->orWhere('promotions.type', 'turbo');
        $posts = Post::orderBy('created_at')->where('posts.moderate_accept', 10);
        if (isset($request->residential_id))
        {
            $promotions = $promotions->where('posts.residential_id', $request->residential_id);
            $posts = $posts->where('posts.residential_id', $request->residential_id);
        }

        $promotions = $promotions->get();
        foreach ($promotions as $post)
            $posts = $posts->where('id', '!=', $post->id);

        if (!isset($request->post_type_id))
            return response()->json([
                'promotions' => PostResource::collection($promotions),
                'posts' => PostResource::collection($posts->get())
            ]);

        return response()->json([
            'promotions' => PostResource::collection($promotions),
            'posts' => PostResource::collection($this->filterPosts($posts, $request->post_type_id)->get())
        ]);
    }

    /**
     * @OA\Get(path="/post/list/map",
     *   tags={"Мое объявление"},
     *   operationId="getPostListMap",
     *   summary="Получить объявления по фильтрам вид карты",
     *     @OA\RequestBody(
     *     description="",
     *          @OA\JsonContent(
     *             @OA\Property(property="post_type_id", type="integer", example="1"),
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  {
     *                      "residential_id": 1,
     *                      "coordinates": "46.409064188282095, 30.721682752654623",
     *                      "total": 1
     *                  }
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function getPostsListMap(Request $request)
    {
        $posts = Post::selectRaw('residential_id, residentials.coordinates, count(posts.id) as total')
                        ->leftJoin('residentials', 'residentials.id', '=', 'posts.residential_id');
        if (isset($request->post_type_id))
            $posts = $this->filterPosts($posts, $request->post_type_id)->groupBy('residential_id');
        else
            $posts = $posts->groupBy('residential_id');

        return response()->json($posts->get());
    }

    /**
     * @OA\Get(path="/post/get",
     *   tags={"Мое объявление"},
     *   operationId="getPost",
     *   summary="Получить объявление",
     *     @OA\RequestBody(
     *     description="",
     *          @OA\JsonContent(
     *             @OA\Property(property="post_id", type="string", example="1"),
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                      "id": 1,
     *                      "address": "ул. Темерязева 7",
     *                      "district": 1,
     *                      "micro_district": 1,
     *                      "residential_id": 1,
     *                      "document_owner": 1,
     *                      "flat_type": 2,
     *                      "count_room": 2,
     *                      "flat_state": 1,
     *                      "square": "72.00",
     *                      "square_kitchen": "20.00",
     *                      "balcony": 1,
     *                      "heating_type": 3,
     *                      "commission": 100000,
     *                      "mortage": 1,
     *                      "connection_type": 1,
     *                      "desc": "Flat is good",
     *                      "gallery": {},
     *                      "price": 34000000,
     *                      "payment_type": 1,
     *                      "post_type_id": 1,
     *                      "views": 0,
     *                      "favorites": 0,
     *                      "moderate_accept": 1,
     *                      "post_type_name": "Вторичный рынок",
     *                      "district_name": "Приморский",
     *                      "micro_district_name": "Центр",
     *                      "residential_name": "Residential",
     *                      "document_owner_name": "Собственность",
     *                      "flat_type_name": "Квартира",
     *                      "plans": {
     *                          {
     *                              "id": 1,
     *                              "name": "Студия"
     *                          },
     *                          {
     *                              "id": 2,
     *                              "name": "Сан. узел"
     *                          }
     *                      },
     *                      "flat_state_name": "Жилое",
     *                      "balcony_name": "Нет",
     *                      "heating_type_name": "Нет",
     *                      "payment_type_name": "Мат. капитал",
     *                      "mortage_name": "Нет",
     *                      "connection_type_name": "Звонок + сообщения",
     *              }
     *          ),
     *        }
     *    ),
     *   @OA\Response(
     *      response="301",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 301,
     *                  "message": "Not found",
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function getPost(Request $request)
    {
        $post = Post::where('id', $request->post_id)->first();
        if ($post != null)
        {
            $post['favorites'] = count(Favorite::where('post_id', $post->id)->get());
            $post['gallery'] = GalleryResource::collection(Gallery::where('gallery', $post->gallery_id)->orderBy('position')->get());
            $post['post_type_name'] = PostType::where('id', $post->post_type_id)->first()->name;
            $post['district_name'] = Expression::where('type', 'districts')->where('const_id', $post->district)->first()->text;
            $post['micro_district_name'] = Expression::where('type', 'micro_districts')->where('const_id', $post->micro_district)->first()->text;;
            $post['residential_name'] = Residential::where('id', $post->residential_id)->first()->name;
            $post['document_owner_name'] = Expression::where('type', 'document_owner')->where('const_id', $post->document_owner)->first()->text;
            $post['flat_type_name'] = Expression::where('type', 'flat_type')->where('const_id', $post->flat_type)->first()->text;
            $post['plans'] = PlanResource::collection(PostPlan::selectRaw('plans.id, plans.name')
                ->where('post_id', $post->id)
                ->leftJoin('plans', 'plans.id', '=', 'post_plans.plan_id')
                ->get());
            $post['flat_state_name'] = Expression::where('type', 'flat_state')->where('const_id', $post->flat_state)->first()->text;
            $post['balcony_name'] = $post->balcony == 1 ? "Нет" : "Да";
            $post['heating_type_name'] = Expression::where('type', 'heating_type')->where('const_id', $post->heating_type)->first()->text;
            $post['payment_type_name'] = Expression::where('type', 'payment_types')->where('const_id', $post->payment_type)->first()->text;
            $post['mortage_name'] = $post->mortage == 1 ? "Нет" : "Да";
            $post['connection_type_name'] = Expression::where('type', 'connection_type')->where('const_id', $post->connection_type)->first()->text;

            Post::where('id', $post->id)->increment('views');
            return response()->json(collect($post));
        }
        return response()->json(['status' => 301, 'message'=>'Not found'], 301);
    }

    /**
     * @OA\Post(path="/post/delete",
     *   tags={"Мое объявление"},
     *   operationId="deletePost",
     *   summary="Удалить объявление",
     *     @OA\RequestBody(
     *     description="",
     *          @OA\JsonContent(
     *             @OA\Property(property="post_id", type="string", example="1"),
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
 *                      "status": 200,
     *                  "message": "Post deleted",
     *              }
     *          ),
     *        }
     *    ),
     *   @OA\Response(
     *      response="301",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 301,
     *                  "message": "Access denied",
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function deletePost(Request $request)
    {
        $post = Post::where('id', $request->post_id)->first();
        if ($post != null && Auth::user()->id == $post->user_id)
        {
            Post::deletePost($post->id);
            return response()->json(['status' => 200, 'message'=>'Post deleted']);
        }
        return response()->json(['status' => 301, 'message'=>'Access denied'], 301);
    }

    /**
     * @OA\Get(path="/post/my_posts",
     *   tags={"Мое объявление"},
     *   operationId="getMyPosts",
     *   summary="Получить объявления",
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                      "id": 1,
     *                      "address": "ул. Темерязева 7",
     *                      "district": 1,
     *                      "micro_district": 1,
     *                      "residential_id": 1,
     *                      "document_owner": 1,
     *                      "flat_type": 2,
     *                      "count_room": 2,
     *                      "flat_state": 1,
     *                      "plans": {
     *                          {
     *                              "id": 1,
     *                              "name": "Студия"
     *                          },
     *                          {
     *                              "id": 2,
     *                              "name": "Сан. узел"
     *                          }
     *                      },
     *                      "square": "72.00",
     *                      "square_kitchen": "20.00",
     *                      "balcony": 1,
     *                      "heating_type": 3,
     *                      "commission": 100000,
     *                      "mortage": 1,
     *                      "connection_type": 1,
     *                      "desc": "Flat is good",
     *                      "gallery": {},
     *                      "price": 34000000,
     *                      "payment_type": 1,
     *                      "post_type_id": 1,
     *                      "views": 0,
     *                      "favorite": 0,
     *                      "moderate_accept": 1,
     *                      "promotions": {
     *                          {
     *                              "id": 1,
     *                              "name": "Турбо",
     *                              "desc": "Поднятие вашего объявления в топ",
     *                              "image": "http://api-swipe.com/images/promotions/promotion-turbo.png",
     *                              "efficiency": 90,
     *                              "price": 499,
     *                              "type": "turbo",
     *                              "value": null
     *                          },
     *                          {
     *                              "id": 2,
     *                              "name": "Поднятие Объявления",
     *                              "desc": "Поднять объявление и закрепить",
     *                              "image": "http://api-swipe.com/images/promotions/promotion-top.png",
     *                              "efficiency": 60,
     *                              "price": 399,
     *                              "type": "color",
     *                              "value": "green"
     *                          }
     *                      }
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function getMyPosts()
    {
        return response()->json(PostResource::collection(Post::where('user_id', Auth::user()->id)->get()));
    }

    /**
     * @OA\Get(path="/post/form/search_residential",
     *   tags={"Мое объявление"},
     *   operationId="getResidential",
     *   summary="Поиск ЖК",
     *   @OA\RequestBody(
     *    description="Слово по которому будет поиск",
     *    @OA\JsonContent(
     *       @OA\Property(property="needle", type="string", example="ЖК"),
     *    ),
     * ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "name": "ЖК Колесный",
     *                   "id": 1
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function getResidentialBySearch(Request $request)
    {
        $residentials = [];
        foreach(Residential::orderBy('name')->get() as $residential)
        {
            if(stripos($residential->name, $request->needle) !== False || $request->needle == '')
            {
                $residentials[]['name'] = $residential->name;
                $residentials[count($residentials)-1]['id'] = $residential->id;
            }
        }

        return response()->json($residentials);
    }

    /**
     * @OA\Get(path="/post/form/create",
     *   tags={"Мое объявление"},
     *   operationId="formPostCreate",
     *   summary="Получить форму для создания объявления",
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "districts": {
     *                      "1": "Центральный",
     *                      "2": "Приморский",
     *                      "3": "Малиновский",
     *                  },
     *                  "micro_districts": {
     *                      "1": "Центр",
     *                      "2": "Парк",
     *                      "3": "Дорожный",
     *                  },
     *                  "post_type": {
     *                      "1": "Вторичный рынок",
     *                      "2": "Новостройки",
     *                      "3": "Все",
     *                      "4": "Коттеджы"
     *                  },
     *                  "residentials": {
     *                      "1": "ЖК Колесный"
     *                  },
     *                  "document_owner": {
     *                      "1": "Собственность",
     *                      "2": "Наследство",
     *                      "3": "Доверенность"
     *                  },
     *                  "flat_type": {
     *                      "1": "Апартаменты",
     *                      "2": "Квартира",
     *                      "3": "Кладовая"
     *                  },
     *                  "plans": {
     *                      "1": "Студия",
     *                      "2": "Сан. узел",
     *                      "3": "Второй выход"
     *                  },
     *                  "flat_state": {
     *                      "1": "Жилое",
     *                      "2": "Требует ремонта",
     *                      "3": "Аварийное",
     *                      "4": "Черновая"
     *                  },
     *                  "balcony": {
     *                      "1": "Нет",
     *                      "10": "Да"
     *                  },
     *                  "heating_type": {
     *                      "1": "Центральное",
     *                      "2": "Газовое",
     *                      "3": "Нет"
     *                  },
     *                  "payment_type": {
     *                      "1": "Мат. капитал",
     *                      "2": "Карта",
     *                      "3": "Банковский перевод"
     *                  },
     *                  "mortage": {
     *                      "1": "Нет",
     *                      "10": "Да",
     *                  },
     *                  "connection_type": {
     *                      "1": "Звонок + сообщения",
     *                      "2": "Сообщение",
     *                      "3": "В офисе"
     *                  }
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function formPostCreate()
    {
        return response()->json($this->getFormParameters());
    }

    /**
     * @OA\Get(path="/post/form/update",
     *   tags={"Мое объявление"},
     *   operationId="formPostUpdate",
     *   summary="Получить форму для редактирования объявления",
     *     @OA\RequestBody(
     *     description="",
     *          @OA\JsonContent(
     *             @OA\Property(property="post_id", type="integer", example="1"),
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "post": {
     *                      "id": 1,
     *                      "address": "ул. Княжеская 7",
     *                      "district": 1,
     *                      "micro_district": 1,
     *                      "residential_id": 1,
     *                      "document_owner": 1,
     *                      "flat_type": 2,
     *                      "count_room": 2,
     *                      "flat_state": 1,
     *                      "plans": {
     *                          {
     *                          "id": 1,
     *                          "name": "Студия"
     *                          },
     *                          {
     *                          "id": 2,
     *                          "name": "Сан. узел"
     *                          }
     *                      },
     *                      "square": "72.00",
     *                      "square_kitchen": "20.00",
     *                      "balcony": 1,
     *                      "heating_type": 3,
     *                      "commission": 100000,
     *                      "mortage": 1,
     *                      "connection_type": 1,
     *                      "desc": "Flat is good",
     *                      "gallery": {},
     *                      "price": 34000000,
     *                      "payment_type": 1,
     *                      "post_type_id": 1,
     *                      "views": 8,
     *                      "moderate_accept": 1,
     *                      "promotions": {
     *                          {
     *                          "id": 1,
     *                          "name": "Турбо",
     *                          "desc": "Поднятие вашего объявления в топ",
     *                          "image": "http://api-swipe.com/images/promotions/promotion-turbo.png",
     *                          "efficiency": 90,
     *                          "price": 499,
     *                          "type": "turbo",
     *                          "value": null
     *                          },
     *                          {
     *                          "id": 2,
     *                          "name": "Поднятие Объявления",
     *                          "desc": "Поднять объявление и закрепить",
     *                          "image": "http://api-swipe.com/images/promotions/promotion-top.png",
     *                          "efficiency": 60,
     *                          "price": 399,
     *                          "type": "color",
     *                          "value": "green"
     *                          }
     *                          }
     *                  },
     *                  "data": {
     *                      "post_type": {
     *                          "1": "Вторичный рынок",
     *                          "2": "Новостройки",
     *                          "3": "Все",
     *                          "4": "Коттеджы"
     *                      },
     *                      "residentials": {
     *                          "1": "ЖК Колесный"
     *                      },
     *                      "document_owner": {
     *                          "1": "Собственность",
     *                          "2": "Наследство",
     *                          "3": "Доверенность"
     *                      },
     *                      "flat_type": {
     *                          "1": "Апартаменты",
     *                          "2": "Квартира",
     *                          "3": "Кладовая"
     *                      },
     *                      "plans": {
     *                          "1": "Студия",
     *                          "2": "Сан. узел",
     *                          "3": "Второй выход"
     *                      },
     *                      "flat_state": {
     *                          "1": "Жилое",
     *                          "2": "Требует ремонта",
     *                          "3": "Аварийное",
     *                          "4": "Черновая"
     *                      },
     *                      "balcony": {
     *                          "1": "Нет",
     *                          "10": "Да"
     *                      },
     *                      "heating_type": {
     *                          "1": "Центральное",
     *                          "2": "Газовое",
     *                          "3": "Нет"
     *                      },
     *                      "payment_type": {},
     *                      "payment_types": {
     *                          "49": "Карта",
     *                          "50": "Наличные"
     *                      },
     *                      "mortage": {
     *                          "1": "Нет",
     *                          "10": "Да"
     *                      },
     *                      "connection_type": {
     *                          "1": "Звонок + сообщения",
     *                          "2": "Сообщение",
     *                          "3": "В офисе"
     *                      }
     *                  }
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function formPostUpdate(Request $request)
    {
        $post = Post::where('id', $request->post_id)->first();
        if ($post != null && Auth::user()->id == $post->user_id)
        {
            Post::where('id', $post->id)->increment('views');
            return response()->json(['post' => new PostResource($post) ,'data' => $this->getFormParameters()]);
        }
        return response()->json(['status' => 301, 'message'=>'Access denied'], 301);
    }

    /**
     * @OA\Post(path="/post/create",
     *   tags={"Мое объявление"},
     *   operationId="getCreatePost",
     *   summary="Создать объявление",
     *     @OA\RequestBody(
     *     description="",
 *              @OA\JsonContent(
     *             @OA\Property(property="address", type="string", example="ул. Темерязева 7"),
     *             @OA\Property(property="district", type="integer", example="1"),
     *             @OA\Property(property="micro_district", type="integer", example="1"),
     *             @OA\Property(property="post_type", type="integer", example="1"),
     *             @OA\Property(property="residential_id", type="integer", example="1"),
     *             @OA\Property(property="document_owner", type="integer", example="1"),
     *             @OA\Property(property="flat_type", type="integer", example="2"),
     *             @OA\Property(property="count_room", type="integer", example="2"),
     *             @OA\Property(property="flat_state", type="integer", example="1"),
     *             @OA\Property(property="plans", type="object", example={"0":1,"1":2}),
     *             @OA\Property(property="square", type="integer", example="72"),
     *             @OA\Property(property="square_kitchen", type="integer", example="20"),
     *             @OA\Property(property="balcony", type="integer", example="1"),
     *             @OA\Property(property="heating_type", type="integer", example="3"),
     *             @OA\Property(property="commission", type="integer", example="100000"),
     *             @OA\Property(property="mortage", type="integer", example="1"),
     *             @OA\Property(property="connection_type", type="integer", example="1"),
     *             @OA\Property(property="desc", type="string", example="Flat is good"),
     *             @OA\Property(property="price", type="integer", example="34000000"),
     *             @OA\Property(property="payment_type", type="integer", example="1"),
     *             @OA\Property(property="photos", type="object", example={"\position (1)\":"file"}),
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 200,
     *                  "message": "Post created",
     *                  "post_id": 23,
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function createPost(Request $request)
    {
        return response()->json(['status' => 200, 'message' => 'Post created', 'post_id' =>
            Post::createPost($request)]);
    }

    /**
     * @OA\Post(path="/post/update",
     *   tags={"Мое объявление"},
     *   operationId="getUpdatePost",
     *   summary="Редактировать объявление",
     *     @OA\RequestBody(
     *     description="",
 *              @OA\JsonContent(
     *             @OA\Property(property="post_id", type="integer", example="1"),
     *             @OA\Property(property="address", type="string", example="ул. Темерязева 7"),
     *             @OA\Property(property="district", type="integer", example="1"),
     *             @OA\Property(property="micro_district", type="integer", example="1"),
     *             @OA\Property(property="post_type", type="integer", example="1"),
     *             @OA\Property(property="residential_id", type="integer", example="1"),
     *             @OA\Property(property="document_owner", type="integer", example="1"),
     *             @OA\Property(property="flat_type", type="integer", example="2"),
     *             @OA\Property(property="count_room", type="integer", example="2"),
     *             @OA\Property(property="flat_state", type="integer", example="1"),
     *             @OA\Property(property="plans", type="object", example={"0":1,"1":2}),
     *             @OA\Property(property="square", type="integer", example="72"),
     *             @OA\Property(property="square_kitchen", type="integer", example="20"),
     *             @OA\Property(property="balcony", type="integer", example="1"),
     *             @OA\Property(property="heating_type", type="integer", example="3"),
     *             @OA\Property(property="commission", type="integer", example="100000"),
     *             @OA\Property(property="mortage", type="integer", example="1"),
     *             @OA\Property(property="connection_type", type="integer", example="1"),
     *             @OA\Property(property="desc", type="string", example="Flat is good"),
     *             @OA\Property(property="price", type="integer", example="34000000"),
     *             @OA\Property(property="payment_type", type="integer", example="1"),
     *             @OA\Property(property="photos", type="object", example={"\position (1)\":"file"}),
     *             @OA\Property(property="delete_photos", type="object", example={"0":"\position (1)\"}),
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 200,
     *                  "message": "Post updated",
     *              }
     *          ),
     *        }
     *    ),
     *   @OA\Response(
     *      response="301",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 301,
     *                  "message": "Access denied",
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function updatePost(Request $request)
    {
        $post = Post::where('id', $request->post_id)->first();
        if ($post != null && Auth::user()->id == $post->user_id)
        {
            Post::updatePost($request, $request->post_id);
            return response()->json(['status' => 200, 'message' => 'Post updated']);
        }

        return response()->json(['status' => 301, 'message' => 'Access denied'],301);
    }

    /**
     * @OA\Get(path="/favorite/post/add",
     *   tags={"Избранное"},
     *   operationId="addPostToFavorite",
     *   summary="Добавить в избранное объявление",
     *     @OA\RequestBody(
     *     description="",
     *          @OA\JsonContent(
     *             @OA\Property(property="post_id", type="integer", example="1"),
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 200,
     *                  "message": "Post added to favorite",
     *              }
     *          ),
     *        }
     *    ),
     *   @OA\Response(
     *      response="301",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 301,
     *                  "message": "Post has already added!",
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function addFavorite(Request $request)
    {
        if (Favorite::where('user_id', Auth::user()->id)->where('post_id', $request->post_id)->first() == null)
        {
            Favorite::insert([
                'user_id' => Auth::user()->id,
                'post_id' => $request->post_id,
                'type' => 'post'
            ]);
            return response()->json(['status' => 200, 'message' => 'Post added to favorite']);
        }
        return response()->json(['status' => 301, 'message' => 'Post has already added!']);
    }

    /**
     * @OA\Get(path="/favorite/post/delete",
     *   tags={"Избранное"},
     *   operationId="deletePostToFavorite",
     *   summary="Убрать из избранных объявление",
     *     @OA\RequestBody(
     *     description="",
     *          @OA\JsonContent(
     *             @OA\Property(property="post_id", type="integer", example="1"),
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 200,
     *                  "message": "Post removed from favorite",
     *              }
     *          ),
     *        }
     *    ),
     *   @OA\Response(
     *      response="301",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 301,
     *                  "message": "Post not found",
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function deleteFavorite(Request $request)
    {
        if (Favorite::where('user_id', Auth::user()->id)->where('post_id', $request->post_id)->first() != null)
        {
            Favorite::where('user_id', Auth::user()->id)->where('post_id', $request->post_id)->delete();
            return response()->json(['status' => 200, 'message' => 'Post removed from favorite']);
        }
        return response()->json(['status' => 301, 'message' => 'Post not found']);
    }

    /**
     * @OA\Get(path="/favorite/list",
     *   tags={"Избранное"},
     *   operationId="listFavorite",
     *   summary="Получить избранные объявления",
     *     @OA\RequestBody(
     *     description="",
     *          @OA\JsonContent(
     *             @OA\Property(property="type", type="string", example="post"),
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                      "address": "ул. Темерязева 7",
     *                      "district": 1,
     *                      "micro_district": 1,
     *                      "residential_id": 1,
     *                      "document_owner": 1,
     *                      "flat_type": 2,
     *                      "count_room": 2,
     *                      "flat_state": 1,
     *                      "plans": {
     *                          {
     *                              "id": 1,
     *                              "name": "Студия"
     *                          },
     *                          {
     *                              "id": 2,
     *                              "name": "Сан. узел"
     *                          }
     *                      },
     *                      "square": "72.00",
     *                      "square_kitchen": "20.00",
     *                      "balcony": 1,
     *                      "heating_type": 3,
     *                      "commission": 100000,
     *                      "mortage": 1,
     *                      "connection_type": 1,
     *                      "desc": "Flat is good",
     *                      "gallery": {},
     *                      "price": 34000000,
     *                      "payment_type": 1,
     *                      "post_type_id": 1,
     *                      "views": 0,
     *                      "moderate_accept": 1,
     *                      "promotions": {
     *                          {
     *                              "id": 1,
     *                              "name": "Турбо",
     *                              "desc": "Поднятие вашего объявления в топ",
     *                              "image": "http://api-swipe.com/images/promotions/promotion-turbo.png",
     *                              "efficiency": 90,
     *                              "price": 499,
     *                              "type": "turbo",
     *                              "value": null
     *                          },
     *                          {
     *                              "id": 2,
     *                              "name": "Поднятие Объявления",
     *                              "desc": "Поднять объявление и закрепить",
     *                              "image": "http://api-swipe.com/images/promotions/promotion-top.png",
     *                              "efficiency": 60,
     *                              "price": 399,
     *                              "type": "color",
     *                              "value": "green"
     *                          }
     *                      }
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function listFavorite(Request $request)
    {
        return response()->json(PostResource::collection(Favorite::selectRaw('posts.*')
            ->join('posts', 'posts.id', '=', 'favorites.post_id')
            ->where('favorites.user_id', Auth::user()->id)
            ->where('favorites.type', $request->type)
            ->get()));
    }

    //Admin
    /**
     * @OA\Get(path="/moderate/post/list",
     *   tags={"Модерация"},
     *   operationId="listModeratePost",
     *   summary="Получить объявления для модерации",
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                      "address": "ул. Темерязева 7",
     *                      "district": 1,
     *                      "micro_district": 1,
     *                      "residential_id": 1,
     *                      "document_owner": 1,
     *                      "flat_type": 2,
     *                      "count_room": 2,
     *                      "flat_state": 1,
     *                      "plans": {
     *                          {
     *                              "id": 1,
     *                              "name": "Студия"
     *                          },
     *                          {
     *                              "id": 2,
     *                              "name": "Сан. узел"
     *                          }
     *                      },
     *                      "square": "72.00",
     *                      "square_kitchen": "20.00",
     *                      "balcony": 1,
     *                      "heating_type": 3,
     *                      "commission": 100000,
     *                      "mortage": 1,
     *                      "connection_type": 1,
     *                      "desc": "Flat is good",
     *                      "gallery": {},
     *                      "price": 34000000,
     *                      "payment_type": 1,
     *                      "post_type_id": 1,
     *                      "views": 0,
     *                      "moderate_accept": 1,
     *                      "promotions": {
     *                          {
     *                              "id": 1,
     *                              "name": "Турбо",
     *                              "desc": "Поднятие вашего объявления в топ",
     *                              "image": "http://api-swipe.com/images/promotions/promotion-turbo.png",
     *                              "efficiency": 90,
     *                              "price": 499,
     *                              "type": "turbo",
     *                              "value": null
     *                          },
     *                          {
     *                              "id": 2,
     *                              "name": "Поднятие Объявления",
     *                              "desc": "Поднять объявление и закрепить",
     *                              "image": "http://api-swipe.com/images/promotions/promotion-top.png",
     *                              "efficiency": 60,
     *                              "price": 399,
     *                              "type": "color",
     *                              "value": "green"
     *                          }
     *                      }
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function listModerPosts()
    {
        return response()->json(PostResource::collection(Post::where('moderate_accept', 5)->get()));
    }

    /**
     * @OA\Get(path="/moderate/post/get",
     *   tags={"Модерация"},
     *   operationId="getModeratePost",
     *   summary="Просмотреть объявление",
     *     @OA\RequestBody(
     *     description="",
     *          @OA\JsonContent(
     *             @OA\Property(property="post_id", type="integer", example="1"),
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                      "address": "ул. Темерязева 7",
     *                      "district": 1,
     *                      "micro_district": 1,
     *                      "residential_id": 1,
     *                      "document_owner": 1,
     *                      "flat_type": 2,
     *                      "count_room": 2,
     *                      "flat_state": 1,
     *                      "plans": {
     *                          {
     *                              "id": 1,
     *                              "name": "Студия"
     *                          },
     *                          {
     *                              "id": 2,
     *                              "name": "Сан. узел"
     *                          }
     *                      },
     *                      "square": "72.00",
     *                      "square_kitchen": "20.00",
     *                      "balcony": 1,
     *                      "heating_type": 3,
     *                      "commission": 100000,
     *                      "mortage": 1,
     *                      "connection_type": 1,
     *                      "desc": "Flat is good",
     *                      "gallery": {},
     *                      "price": 34000000,
     *                      "payment_type": 1,
     *                      "post_type_id": 1,
     *                      "views": 0,
     *                      "moderate_accept": 1,
     *                      "promotions": {
     *                          {
     *                              "id": 1,
     *                              "name": "Турбо",
     *                              "desc": "Поднятие вашего объявления в топ",
     *                              "image": "http://api-swipe.com/images/promotions/promotion-turbo.png",
     *                              "efficiency": 90,
     *                              "price": 499,
     *                              "type": "turbo",
     *                              "value": null
     *                          },
     *                          {
     *                              "id": 2,
     *                              "name": "Поднятие Объявления",
     *                              "desc": "Поднять объявление и закрепить",
     *                              "image": "http://api-swipe.com/images/promotions/promotion-top.png",
     *                              "efficiency": 60,
     *                              "price": 399,
     *                              "type": "color",
     *                              "value": "green"
     *                          }
     *                      }
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function getModerPost(Request $request)
    {
        $post = Post::where('id', $request->post_id)->where('moderate_accept', 5)->first();
        if ($post == null)
            return response()->json(['status' => 301, 'message' => 'Post not exist or not require moderation']);

        return response()->json(new PostResource($post));
    }

    /**
     * @OA\Post(path="/moderate/post/accept",
     *   tags={"Модерация"},
     *   operationId="acceptPost",
     *   summary="Принять объявление на публикацию",
     *     @OA\RequestBody(
     *     description="",
     *          @OA\JsonContent(
     *             @OA\Property(property="post_id", type="integer", example="1"),
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 200,
     *                  "message": "Post accepted"
     *              }
     *          ),
     *        }
     *    ),
     *   @OA\Response(
     *      response="301",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 301,
     *                  "message": "Post not exist or not require moderation"
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function acceptPost(Request $request)
    {
        $post = Post::where('id', $request->post_id)->where('moderate_accept', 5)->first();
        if ($post == null)
            return response()->json(['status' => 301, 'message' => 'Post not exist or not require moderation'], 301);
        Post::acceptPost($request->post_id);

        return response()->json(['status' => 200, 'message' => 'Post accepted']);
    }

    /**
     * @OA\Post(path="/moderate/post/reject",
     *   tags={"Модерация"},
     *   operationId="rejectPost",
     *   summary="Отказать объявлению в публикации",
     *     @OA\RequestBody(
     *     description="",
     *          @OA\JsonContent(
     *             @OA\Property(property="post_id", type="integer", example="1"),
     *             @OA\Property(property="message", type="string", example="3"),
     *          ),
     *      ),
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 200,
     *                  "message": "Post rejected"
     *              }
     *          ),
     *        }
     *    ),
     *   @OA\Response(
     *      response="301",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  "status": 301,
     *                  "message": "Post not exist or not require moderation"
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function rejectPost(Request $request)
    {
        $post = Post::where('id', $request->post_id)->where('moderate_accept', 5)->first();
        if ($post == null)
            return response()->json(['status' => 301, 'message' => 'Post not exist or not require moderation'], 301);

        Post::rejectPost($request->post_id, $request->message);
        return response()->json(['status' => 200, 'message' => 'Post rejected']);
    }

    /**
     * @OA\Get(path="/moderate/post/reject/phrases",
     *   tags={"Модерация"},
     *   operationId="rejectPhrases",
     *   summary="Причины отказа",
     *   @OA\Response(
     *      response="200",
     *      description="",
     *      content={
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             example={
     *                  {
     *                      "id": 1,
     *                      "text": "Некорректная цена"
     *                  },
     *                  {
     *                      "id": 2,
     *                      "text": "Некорректное описание"
     *                  }
     *              }
     *          ),
     *        }
     *    ),
     * )
     */
    public function rejectPostPhrases()
    {
        return response()->json(collect(Expression::selectRaw('const_id as id, text')->where('type', 'moderate_phrase')->get()));
    }
}
