<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserCabinetResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'contacts' => [
                'firstname' => $this->firstname,
                'lastname' => $this->lastname,
                'phone' => $this->phone,
                'email' => $this->email,
                'image' => isset($this->image) ? asset($this->image) : null
                ],
            'agent_contacts' => [
                'firstname' => $this->agent_firstname,
                'lastname' => $this->agent_lastname,
                'phone' => $this->agent_phone,
                'email' => $this->agent_email,
                ],
            'signed_before' => $this->signed_before->format('d.m.Y'),
            'notifications' => $this->notifications,
            'turn_on_agent' => $this->turn_on_agent
        ];
    }
}
