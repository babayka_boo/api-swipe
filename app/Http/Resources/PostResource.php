<?php

namespace App\Http\Resources;

use App\Models\Favorite;
use App\Models\Gallery;
use App\Models\PostPlan;
use App\Models\PostPromotion;
use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        PostPromotion::where('date_end', '<', now()->format('Y-m-d'))->delete();
        return [
            'id' => $this->id,
            'address' => $this->address,
            'district' => $this->district,
            'micro_district' => $this->micro_district,
            'residential_id' => $this->residential_id,
            'document_owner' => $this->document_owner,
            'flat_type' => $this->flat_type,
            'count_room' => $this->count_room,
            'flat_state' => $this->flat_state,
            'plans' => PlanResource::collection(PostPlan::selectRaw('plans.id, plans.name')->where('post_id', $this->id)->leftJoin('plans', 'plans.id', '=', 'post_plans.plan_id')->get()),
            'square' => $this->square,
            'square_kitchen' => $this->square_kitchen,
            'balcony' => $this->balcony,
            'heating_type' => $this->heating_type,
            'commission' => $this->commission,
            'mortage' => $this->mortage,
            'connection_type' => $this->connection_type,
            'desc' => $this->desc,
            'gallery' => GalleryResource::collection(Gallery::where('gallery', $this->gallery_id)->orderBy('position')->get()),
            'price' => $this->price,
            'payment_type' => $this->payment_type,
            'post_type_id' => $this->post_type_id,
            'views' => $this->views ?? 0,
            'favorites' => count(Favorite::where('post_id', $this->id)->get()),
            'moderate_accept' => $this->moderate_accept ?? 5,
            'promotions' => PromotionResource::collection(PostPromotion::selectRaw('promotions.*, post_promotions.value')
                ->join('promotions', 'promotions.id', '=', 'post_promotions.promotion_id')
                ->where('post_promotions.post_id', $this->id)
                ->get())
        ];
    }
}
