<?php

namespace App\Http\Resources;

use App\Models\Document;
use App\Models\Gallery;
use Illuminate\Http\Resources\Json\JsonResource;

class ResidentialResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'coordinates' => $this->coordinates,
            'builder_id' => $this->builder_id,
            'name' => $this->name,
            'desc' => $this->desc,
            'infrastructure' => [
                'status' => $this->status,
                'house_type' => $this->house_type,
                'house_class' => $this->house_class,
                'technology_build' => $this->technology_build,
                'territory' => $this->territory,
                'distance_sea' => $this->distance_sea,
                'communal' => $this->communal,
                'height_ceiling' => $this->height_ceiling,
                'gas' => $this->gas,
                'heating' => $this->heating,
                'sewerage' => $this->sewerage,
                'water' => $this->water,
            ],
            'reg_pay' => [
                'registration' => $this->registration,
                'mortage' => $this->mortage,
                'flat_type' => $this->flat_type,
                'sum_contract' => $this->sum_contract,
            ],
            'center' => [
                'firstname' => $this->center_firstname,
                'lastname' => $this->center_lastname,
                'phone' => $this->center_phone,
                'email' => $this->center_email,
            ],
            'photos' => GalleryResource::collection(Gallery::where('gallery', $this->gallery_id)->get()),
            'documents' => [
                'excels' => DocumentResource::collection(Document::selectRaw('id, url')->where('residential_id', $this->id)->where('type', 'excel')->get()),
                'pdf' => DocumentResource::collection(Document::selectRaw('id, url')->where('residential_id', $this->id)->where('type', 'pdf')->get())
            ]
        ];
    }
}
