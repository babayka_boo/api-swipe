<?php

namespace Database\Seeders;

use App\Models\Advantage;
use App\Models\Residential;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::insert([
            'id' => 1,
            'email' => 'luckymr304@gmail.com',
            'phone' => '0665051158',
            'firstname' => 'Vova',
            'lastname' => 'User',
            'agent_email' => 'luckymr304@gmail.com',
            'agent_phone' => '0665051158',
            'agent_firstname' => 'Vova',
            'agent_lastname' => 'User',
            'notifications' => 1,
            'signed_before' => now()->modify('+1 year'),
            'blacklist' => 1,
            'turn_on_agent' => 1,
            'api_token' => 'fe6f411ed241a6b98a2829ff6ad0ba2b38808b63ebdbcd4effe4f59ff51269da'
        ]);
        \App\Models\Admin::insert([
            'id' => 1,
            'email' => 'luckymr304@gmail.com',
            'phone' => '0665051159',
            'firstname' => 'Vova',
            'lastname' => 'User',
            'api_token' => '0c60bf7a5ce2b6b11e39f8632ffa15d7c330b34dbdc1bb071c0115c5a1830eab'
        ]);
        \App\Models\Builder::insert([
            'id' => 1,
            'email' => 'luckymr304@gmail.com',
            'phone' => '0665051160',
            'firstname' => 'Vova',
            'lastname' => 'User',
            'api_token' => '6610faefaac9f7894f1076b24b13b545410794fe4088f1d84963966eef14664c'
        ]);
        Residential::factory()
            ->count(3)
            ->create();

        \App\Models\User::factory()
            ->count(3)
            ->has(\App\Models\Post::factory()
                ->count(4))
            ->create();
    }
}
