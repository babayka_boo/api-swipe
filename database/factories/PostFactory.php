<?php

namespace Database\Factories;

use App\Models\PostType;
use App\Models\Residential;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'address' => $this->faker->address,
            'district' => $this->faker->numberBetween(1,3),
            'micro_district' => $this->faker->numberBetween(1,3),
            'residential_id' => Residential::all()->random()->id,
            'document_owner' => 1,
            'flat_type' => $this->faker->numberBetween(1,3),
            'count_room' => $this->faker->numberBetween(1,3),
            'flat_state' => $this->faker->numberBetween(1,3),
            'square' => $this->faker->numberBetween(20,60),
            'square_kitchen' => $this->faker->numberBetween(10,20),
            'balcony' => $this->faker->numberBetween(1,10),
            'heating_type' => $this->faker->numberBetween(1, 3),
            'commission' => $this->faker->numberBetween(10000,1000000),
            'connection_type' => $this->faker->numberBetween(1,3),
            'desc' => $this->faker->text('500'),
            'gallery_id' => null,
            'mortage' => 1,
            'price' => $this->faker->numberBetween(1000000,3000000),
            'payment_type' => $this->faker->numberBetween(1,2),
            'post_type_id' => PostType::all()->random()->id,
            'views'  => 0,
            'moderate_accept' => 5,
            'moderate_message' => null
        ];
    }
}
