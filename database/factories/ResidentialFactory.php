<?php

namespace Database\Factories;

use App\Models\Builder;
use Illuminate\Database\Eloquent\Factories\Factory;

class ResidentialFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'builder_id' => Builder::all()->random()->id,
            'coordinates' => "40.24534, 35.2411",
            'name' => $this->faker->name,
            'desc' => $this->faker->text,
            'status' => $this->faker->numberBetween(1,2),
            'residential_type' => $this->faker->numberBetween(1,2),
            'house_class' => $this->faker->numberBetween(1,2),
            'technology_build' => $this->faker->text,
            'territory' => $this->faker->numberBetween(1,2),
            'distance_sea' => $this->faker->numberBetween(10, 5000),
            'communal' => $this->faker->numberBetween(1,2),
            'height_ceiling' => $this->faker->randomFloat(2, 3, 6),
            'gas' => $this->faker->numberBetween(1,2),
            'heating' => $this->faker->numberBetween(1,3),
            'sewerage' => $this->faker->numberBetween(1,2),
            'water' => $this->faker->numberBetween(1,2),
            'registration' => $this->faker->word,
            'mortage' => 1,
            'flat_type' => $this->faker->numberBetween(1,3),
            'sum_contract' => $this->faker->numberBetween(1,2),
            'center_firstname' => $this->faker->firstName,
            'center_lastname' => $this->faker->lastName,
            'center_phone' => $this->faker->phoneNumber,
            'center_email' => $this->faker->email,
        ];
    }
}
