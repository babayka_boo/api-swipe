<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResidentials extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('residentials', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('builder_id')->index();
            $table->foreign('builder_id')->references('id')->on('builders');
            $table->string('coordinates');
            $table->string('name');
            $table->text('desc');
            $table->smallInteger('status');
            $table->smallInteger('residential_type');
            $table->smallInteger('house_class');
            $table->text('technology_build');
            $table->smallInteger('territory');
            $table->integer('distance_sea');
            $table->smallInteger('communal');
            $table->decimal('height_ceiling');
            $table->smallInteger('gas');
            $table->smallInteger('heating');
            $table->smallInteger('sewerage');
            $table->smallInteger('water');
            $table->string('registration');
            $table->smallInteger('mortage');
            $table->smallInteger('flat_type');
            $table->smallInteger('sum_contract');
            $table->string('center_firstname');
            $table->string('center_lastname');
            $table->string('center_phone');
            $table->string('center_email');
            $table->unsignedBigInteger('gallery_id')->index()->nullable();
            $table->foreign('gallery_id')->references('id')->on('galleries');
            $table->smallInteger('count_sections')->default(4);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('residentials');
    }
}
