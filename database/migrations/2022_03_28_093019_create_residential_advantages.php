<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResidentialAdvantages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('residential_advantages', function (Blueprint $table) {
            $table->unsignedBigInteger('residential_id')->index();
            $table->foreign('residential_id')->references('id')->on('residentials');
            $table->unsignedBigInteger('advantage_id')->index();
            $table->foreign('advantage_id')->references('id')->on('advantages');
            $table->primary(array('residential_id', 'advantage_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('residential_advantages');
    }
}
