<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostPromotions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_promotions', function (Blueprint $table) {
            $table->unsignedBigInteger('promotion_id')->index();
            $table->foreign('promotion_id')->references('id')->on('promotions');
            $table->unsignedBigInteger('post_id')->index();
            $table->foreign('post_id')->references('id')->on('posts');
            $table->primary(array('post_id', 'promotion_id'));
            $table->string('value')->nullable();
            $table->date('date_end');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_promotions');
    }
}
