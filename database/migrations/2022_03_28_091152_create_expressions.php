<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateExpressions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expressions', function (Blueprint $table) {
            $table->id();
            $table->smallInteger('const_id');
            $table->string('text');
            $table->string('type');
        });
        DB::statement("INSERT INTO `expressions` (`id`, `const_id`, `text`, `type`) VALUES
                                (1, 1, 'Мне', 'notifications'),
                                (2, 2, 'Мне и агенту', 'notifications'),
                                (3, 3, 'Агенту', 'notifications'),
                                (4, 4, 'Отключить', 'notifications'),
                                (5, 1, 'Собственность', 'document_owner'),
                                (6, 2, 'Наследство', 'document_owner'),
                                (7, 3, 'Доверенность', 'document_owner'),
                                (8, 1, 'Апартаменты', 'flat_type'),
                                (9, 2, 'Квартира', 'flat_type'),
                                (10, 3, 'Кладовая', 'flat_type'),
                                (11, 1, 'Жилое', 'flat_state'),
                                (12, 2, 'Требует ремонта', 'flat_state'),
                                (13, 3, 'Аварийное', 'flat_state'),
                                (14, 4, 'Черновая', 'flat_state'),
                                (15, 1, 'Центральное', 'heating_type'),
                                (16, 2, 'Газовое', 'heating_type'),
                                (17, 3, 'Нет', 'heating_type'),
                                (18, 1, 'Звонок + сообщения', 'connection_type'),
                                (19, 2, 'Сообщение', 'connection_type'),
                                (20, 3, 'В офисе', 'connection_type'),
                                (21, 1, 'Подарок при покупке', 'promotion_phrase'),
                                (22, 2, 'Возможен торг', 'promotion_phrase'),
                                (23, 3, 'Квартира у моря', 'promotion_phrase'),
                                (24, 4, 'В спальном районе', 'promotion_phrase'),
                                (25, 5, 'Вам повезло с ценой', 'promotion_phrase'),
                                (26, 6, 'Для большой семьи', 'promotion_phrase'),
                                (27, 7, 'Семейное гнездышко', 'promotion_phrase'),
                                (28, 8, 'Отдельная парковка', 'promotion_phrase'),
                                (29, 1, 'Некорректная цена', 'moderate_phrase'),
                                (30, 2, 'Некорректное фото', 'moderate_phrase'),
                                (31, 3, 'Некорректное описание', 'moderate_phrase'),
                                (32, 1, 'Квартиры', 'residential_status'),
                                (33, 2, 'Апартаменты', 'residential_status'),
                                (34, 1, 'Многоквартирный', 'residential_type'),
                                (35, 2, 'Апартаментный', 'residential_type'),
                                (36, 1, 'Элитный', 'house_class'),
                                (37, 2, 'Бюджетный', 'house_class'),
                                (38, 1, 'Закрытая охраняемая', 'territory'),
                                (39, 2, 'Открытая', 'territory'),
                                (40, 1, 'Платежи', 'communal_payments'),
                                (41, 2, 'Нет', 'territory'),
                                (42, 1, 'Центральный', 'gas_types'),
                                (43, 2, 'Нет', 'gas_types'),
                                (47, 1, 'Центральное', 'water_types'),
                                (48, 2, 'Нет', 'water_types'),
                                (49, 1, 'Мат. капитал', 'payment_types'),
                                (50, 2, 'Карта', 'payment_types'),
                                (51, 1, 'Центральное', 'sewerage_types'),
                                (52, 2, 'Нет', 'sewerage_types'),
                                (53, 1, 'Приморский', 'districts'),
                                (54, 2, 'Киевский', 'districts'),
                                (55, 3, 'Малиновский', 'districts'),
                                (56, 1, 'Центр', 'micro_districts'),
                                (57, 2, 'Парк', 'micro_districts'),
                                (58, 3, 'Дорожный', 'micro_districts');");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expressions');
    }
}
