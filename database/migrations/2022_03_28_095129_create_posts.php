<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePosts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->index();
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('address');
            $table->smallInteger('district');
            $table->smallInteger('micro_district');
            $table->unsignedBigInteger('residential_id')->index();
            $table->foreign('residential_id')->references('id')->on('residentials');
            $table->smallInteger('document_owner');
            $table->smallInteger('flat_type');
            $table->integer('count_room');
            $table->smallInteger('flat_state');
            $table->decimal('square');
            $table->decimal('square_kitchen');
            $table->smallInteger('balcony');
            $table->smallInteger('heating_type');
            $table->integer('commission');
            $table->smallInteger('connection_type');
            $table->text('desc');
            $table->unsignedBigInteger('gallery_id')->index()->nullable();
            $table->foreign('gallery_id')->references('id')->on('galleries');
            $table->smallInteger('mortage');
            $table->integer('price');
            $table->integer('payment_type');
            $table->unsignedBigInteger('post_type_id')->index();
            $table->foreign('post_type_id')->references('id')->on('post_types');
            $table->bigInteger('views');
            $table->smallInteger('moderate_accept')->default(5);
            $table->text('moderate_message')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
