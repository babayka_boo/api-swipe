<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filters', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->index();
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedBigInteger('post_type_id')->index();
            $table->foreign('post_type_id')->references('id')->on('post_types');
            $table->primary(array('user_id', 'post_type_id'));
            $table->smallInteger('district')->nullable();
            $table->smallInteger('micro_district')->nullable();
            $table->integer('count_room')->nullable();
            $table->integer('price_from')->nullable();
            $table->integer('price_to')->nullable();
            $table->decimal('square_from')->nullable();
            $table->decimal('square_to')->nullable();
            $table->smallInteger('flat_type')->nullable();
            $table->smallInteger('mortage')->nullable();
            $table->smallInteger('flat_state')->nullable();
            $table->smallInteger('save_filter');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('filters');
    }
}
