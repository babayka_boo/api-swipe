<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('firstname');
            $table->string('lastname');
            $table->string('email');
            $table->string('phone');
            $table->string('image')->nullable();
            $table->string('agent_firstname');
            $table->string('agent_lastname');
            $table->string('agent_email');
            $table->string('agent_phone');
            $table->smallInteger('notifications');
            $table->date('signed_before');
            $table->smallInteger('blacklist');
            $table->smallInteger('turn_on_agent');
            $table->string('api_token');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
