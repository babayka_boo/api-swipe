<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreatePromotions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotions', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('desc');
            $table->string('image');
            $table->integer('price');
            $table->smallInteger('efficiency');
            $table->string('type');
        });
        DB::statement("INSERT INTO `promotions` (`id`, `name`, `desc`, `image`, `price`, `efficiency`, `type`) VALUES
(1, 'Турбо', 'Поднятие вашего объявления в топ', 'images/promotions/promotion-turbo.png', 499, 90, 'turbo'),
(2, 'Поднятие Объявления', 'Поднять объявление и закрепить', 'images/promotions/promotion-top.png', 399, 60, 'top'),
(3, 'Большое объявление', 'Ваше объявление по размеру будет равно двум обычным', 'images/promotions/promotion-big.png', 299, 60, 'big'),
(4, 'Выделить цветом', 'Цветовая палитра вашего объявления', 'images/promotions/promotion_color.png', 299, 60, 'color');");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotions');
    }
}
