<?php

namespace Tests\Unit;

use App\Models\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class CabinetTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_userCabinet()
    {
        $headers = ['Authorization' => 'Bearer 1O96tGAa8rPoR4QyVL4vmtuJL7GkHyTOMyp5WxMOp7IbSuyjMd2mRVknFKBa'];
        $response = $this->json('post', asset('/api/cabinet/user/update'), [
            "firstname" => "User",
            "lastname" => "Usual",
            "phone" => "0663333333",
            "email" => "luckymr304@gmail.com",
            'image' => UploadedFile::fake()->image('avatar.jpg'),
            "agent_firstname" => "User",
            "agent_lastname" => "User",
            "agent_phone" => "0663333333",
            "agent_email" => "luckymr304@gmail.com",
            "signed_before" => now()->modify('+1 year')->format('Y-m-d'),
            "notifications" => 10,
            "turn_on_agent" => 0
        ], $headers);
        $this->assertEquals(200, $response->status());

        $response = $this->json('get', asset('/api/cabinet/user/get'), [], $headers);
        $this->assertEquals(200, $response->status());
        $this->assertNotEmpty($response->json('contacts'));
        $this->assertNotEmpty($response->json('agent_contacts'));

        $this->assertEquals('User', $response->json('contacts')['firstname']);
        $this->assertEquals('Usual', $response->json('contacts')['lastname']);
        $this->assertEquals('0663333333', $response->json('contacts')['phone']);
        $this->assertEquals('luckymr304@gmail.com', $response->json('contacts')['email']);

        $this->assertNotNull($response->json('contacts')['image']);
        Storage::delete($response->json('contacts')['image']);

        $this->assertEquals('User', $response->json('agent_contacts')['firstname']);
        $this->assertEquals('User', $response->json('agent_contacts')['lastname']);
        $this->assertEquals('0663333333', $response->json('agent_contacts')['phone']);
        $this->assertEquals('luckymr304@gmail.com', $response->json('agent_contacts')['email']);

        $this->assertEquals(now()->modify('+1 year')->format('d.m.Y'), $response->json('signed_before'));
        $this->assertEquals(10, $response->json('notifications'));
        $this->assertEquals(0, $response->json('turn_on_agent'));
    }

    public function test_builderCabinet()
    {
        $headers = ['Authorization' => 'Bearer XdsaNzvrZwmhAuHPB9Qn9BZbKpwXcoAaAA8pEDX2HleM7soPAZctlp10DoJa'];
        $response = $this->json('get', asset('/api/cabinet/builder/get'), [], $headers);
        $this->assertEquals(200, $response->status());
        $this->assertNotNull($response->json('builder')['id']);
        $this->assertNotNull($response->json('builder')['lastname']);
        $this->assertNotNull($response->json('builder')['firstname']);
        $this->assertNotNull($response->json('builder')['email']);
        $this->assertNotNull($response->json('builder')['phone']);

        $response = $this->json('post', asset('/api/cabinet/builder/update'), [
            'lastname' => 'Colson',
            'firstname' => 'Jack',
            'email' => 'nonemail@gmail.com',
            'phone' => '1234567891',
            'image' => UploadedFile::fake()->image('avatar.jpg')
        ], $headers);
        $this->assertEquals(200, $response->status());

        $response = $this->json('get', asset('/api/cabinet/builder/get'), [], $headers);
        $this->assertEquals(200, $response->status());

        $this->assertEquals('Colson', $response->json('builder')['lastname']);
        $this->assertEquals('Jack', $response->json('builder')['firstname']);
        $this->assertEquals('nonemail@gmail.com', $response->json('builder')['email']);
        $this->assertEquals('1234567891', $response->json('builder')['phone']);
        $this->assertNotNull($response->json('builder')['image']);
        Storage::delete($response->json('builder')['image']);
    }

    public function test_adminCabinet()
    {
        $headers = ['Authorization' => 'Bearer wa5654vFXD9QV9s46Q7k44fzIFTzldE5UEAPSTijAPpTt04Ehbj9fdnn7WxX'];
        $response = $this->json('get', asset('/api/cabinet/admin/get'), [], $headers);
        $this->assertEquals(200, $response->status());
        $this->assertNotNull($response->json('id'));
        $this->assertNotNull($response->json('lastname'));
        $this->assertNotNull($response->json('firstname'));
        $this->assertNotNull($response->json('email'));
        $this->assertNotNull($response->json('phone'));

        $response = $this->json('post', asset('/api/cabinet/admin/update'), [
            'lastname' => 'Colson',
            'firstname' => 'Jack',
            'email' => 'nonemail@gmail.com',
            'phone' => '1234567891',
            'image' => UploadedFile::fake()->image('avatar.jpg')
        ], $headers);
        $this->assertEquals(200, $response->status());

        $response = $this->json('get', asset('/api/cabinet/admin/get'), [], $headers);
        $this->assertEquals(200, $response->status());

        $this->assertEquals('Colson', $response->json('lastname'));
        $this->assertEquals('Jack', $response->json('firstname'));
        $this->assertEquals('nonemail@gmail.com', $response->json('email'));
        $this->assertEquals('1234567891', $response->json('phone'));
        $this->assertNotNull($response->json('image'));
        Storage::delete($response->json('image'));
    }
}
