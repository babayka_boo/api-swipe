<?php

namespace Tests\Unit;

use App\Models\Notary;
use App\Models\Post;
use App\Models\User;
use Tests\TestCase;


class AdminTest extends TestCase
{
    private $headers;
    public function setUp(): void
    {
        parent::setUp();
        $this->headers = ['Authorization' => 'Bearer wa5654vFXD9QV9s46Q7k44fzIFTzldE5UEAPSTijAPpTt04Ehbj9fdnn7WxX'];
    }

    public function test_getPostList()
    {
        $response = $this->json('get', asset('/api/moderate/post/list'), [], $this->headers);
        $this->assertEquals(200, $response->status());
        $this->assertEquals(5, Post::where('id',$response->decodeResponseJson()[0]['id'])->first()->moderate_accept);
    }

    public function test_accept()
    {
        $post = Post::where('moderate_accept', 5)->first();
        $response = $this->json('post', asset('/api/moderate/post/accept'), ['post_id' => $post->id], $this->headers);
        $this->assertEquals(200, $response->status());
        $post = Post::where('id', $post->id)->first();
        $this->assertEquals(10, $post->moderate_accept);
    }

    public function test_reject()
    {
        $post = Post::where('moderate_accept', 5)->first();
        $response = $this->json('post', asset('/api/moderate/post/reject'), ['post_id' => $post->id, 'message' => 1], $this->headers);
        $this->assertEquals(200, $response->status());
        $post = Post::where('id', $post->id)->first();
        $this->assertEquals(1, $post->moderate_accept);
        $this->assertEquals(1, $post->moderate_message);
    }

    public function test_userToBlacklist()
    {
        $user = User::all()->random();
        $response = $this->json('post', asset('/api/user/add_to_blacklist'), ['user_id' => $user->id], $this->headers);
        $this->assertEquals(200, $response->status());

        $user = User::where('id', $user->id)->first();
        $this->assertEquals(10, $user->blacklist);
    }

    public function test_userFromBlacklist()
    {
        User::where('id', 1)->update([
            'blacklist' => 10,
        ]);
        $user = User::where('id', 1)->first();
        $response = $this->json('post', asset('/api/user/remove_to_blacklist'), ['user_id' => $user->id], $this->headers);
        $this->assertEquals(200, $response->status());

        $user = User::where('id', $user->id)->first();
        $this->assertEquals(1, $user->blacklist);
    }

    public function test_notary()
    {
        $response = $this->json('post', asset('/api/notary/admin/add'), [
            "firstname" => "John",
            "lastname" => "Bonny",
            "phone" => "095331640",
            "email" => "notary@gmail.com"
        ], $this->headers);
        $this->assertEquals(200, $response->status());
        $this->assertNotNull($response->json('notary_id'));

        $notary = Notary::where('id', $response->json('notary_id'))->first();
        $this->assertNotNull($notary);
        $this->assertEquals("John", $notary->firstname);
        $this->assertEquals("Bonny", $notary->lastname);
        $this->assertEquals("095331640", $notary->phone);
        $this->assertEquals("notary@gmail.com", $notary->email);

        $response = $this->json('post', asset('/api/notary/admin/update'), [
            "notary_id" => $notary->id,
            "firstname" => "John",
            "lastname" => "Bonny",
            "phone" => "066331640",
            "email" => "notary@gmail.com"
        ], $this->headers);
        $this->assertEquals(200, $response->status());

        $notary = Notary::where('id', $notary->id)->first();
        $this->assertEquals("066331640", $notary->phone);

        $response = $this->json('post', asset('/api/notary/admin/delete'), [
            "notary_id" => $notary->id
        ], $this->headers);
        $this->assertEquals(200, $response->status());
        $this->assertNull(Notary::where('id', $notary->id)->first());

    }


}
