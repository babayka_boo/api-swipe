<?php

namespace Tests\Unit;


use Tests\TestCase;

class AuthTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_authorization()
    {
        $response = $this->json('post', 'http://api-swipe.com/api/login', [
            "phone" => "0665051158"
        ]);
        $this->assertEquals(200, $response->status());
    }

    public function test_authorizationError()
    {
        $response = $this->json('post', 'http://api-swipe.com/api/login', [
            "phone" => "0666661158"
        ]);
        $this->assertNotEquals(200, $response->status());
    }
}
