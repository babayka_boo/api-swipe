<?php

namespace Tests\Unit;

use App\Models\Filter;
use Tests\TestCase;

class FilterTest extends TestCase
{
    private $headers;
    public function setUp(): void
    {
        parent::setUp();
        $this->headers = ['Authorization' => 'Bearer 1O96tGAa8rPoR4QyVL4vmtuJL7GkHyTOMyp5WxMOp7IbSuyjMd2mRVknFKBa'];
    }

    public function test_filter_set()
    {
        $response = $this->json('post', asset('/api/filter/set'), [
            "post_type_id" => 1,
            "district" => 1,
            "micro_district" => 1,
            "count_room" => 1,
            "price_from" => 0,
            "price_to" => 4000000,
            "square_from" => 50,
            "square_to" => 70,
            "flat_type" => 1,
            "mortage" => 10,
            "state" => 1,
            "save_filter" => 10
        ], $this->headers);
        $this->assertEquals(200, $response->status());

        $response = $this->json('get', asset('/api/filter/get'), [
            "post_type_id" => 1
        ], $this->headers);
        $this->assertEquals(200, $response->status());

        $filter = $response->decodeResponseJson()['filter'];
        $this->assertEquals(1, $filter['post_type_id']);
        $this->assertEquals(1, $filter['district']);
        $this->assertEquals(1, $filter['micro_district']);
        $this->assertEquals(1, $filter['count_room']);
        $this->assertEquals(0, $filter['price_from']);
        $this->assertEquals(4000000, $filter['price_to']);
        $this->assertEquals(50, $filter['square_from']);
        $this->assertEquals(70, $filter['square_to']);
        $this->assertEquals(1, $filter['flat_type']);
        $this->assertEquals(10, $filter['mortage']);
        $this->assertEquals(1, $filter['flat_state']);
        $this->assertEquals(10, $filter['save_filter']);
    }
}
