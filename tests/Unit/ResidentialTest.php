<?php

namespace Tests\Unit;


use App\Models\Post;
use App\Models\Residential;
use App\Models\ResidentialAdvantage;
use Tests\TestCase;

class ResidentialTest extends TestCase
{
    private $headers;
    public function setUp(): void
    {
        parent::setUp();
        $this->headers = ['Authorization' => 'Bearer XdsaNzvrZwmhAuHPB9Qn9BZbKpwXcoAaAA8pEDX2HleM7soPAZctlp10DoJa'];
    }

    public function test_residential()
    {
        $response = $this->json('post', asset('/api/residential/create'), [
            "coordinates" => "20.12, 40.1",
            "name" => "Residential",
            "desc" => "Cool",
            "residential_status" => 1,
            "residential_type" => 1,
            "house_class" => 2,
            "technology_build" => "Rock and Panel",
            "territory" => 1,
            "distance_sea" => 1000,
            "communal_payments" => 1,
            "height_ceiling" => 3.4,
            "gas_types" => 1,
            "heating_type" => 2,
            "sewerage_types" => 1,
            "water_types" => 1,
            "registration" => "Simply",
            "mortage" => 1,
            "flat_type" => 2,
            "sum_contract" => 10,
            "center_firstname" => "Vika",
            "center_lastname" => "Blade",
            "center_phone" => "0987836898",
            "center_email" => "vika@gmail.com"
        ], $this->headers);
        $this->assertEquals(200, $response->status());

        $residential = Residential::where('id', $response->json('residential_id'))->first();
        $this->assertEquals("20.12, 40.1", $residential->coordinates);
        $this->assertEquals("Residential", $residential->name);
        $this->assertEquals("Cool", $residential->desc);
        $this->assertEquals(1, $residential->status);
        $this->assertEquals("vika@gmail.com", $residential->center_email);


        $response = $this->json('post', asset('/api/residential/update'), [
            "residential_id" => $residential->id,
            "coordinates" => "20.12, 40.1",
            "name" => "RD",
            "desc" => "Cool",
            "residential_status" => 1,
            "residential_type" => 1,
            "house_class" => 2,
            "technology_build" => "Rock and Panel",
            "territory" => 1,
            "distance_sea" => 1000,
            "communal_payments" => 1,
            "height_ceiling" => 3.4,
            "gas_types" => 1,
            "heating_type" => 2,
            "sewerage_types" => 1,
            "water_types" => 1,
            "registration" => "Simply",
            "mortage" => 1,
            "flat_type" => 2,
            "sum_contract" => 10,
            "center_firstname" => "Vika",
            "center_lastname" => "Blade",
            "center_phone" => "0987836898",
            "center_email" => "vika@gmail.com"
        ], $this->headers);
        $this->assertEquals(200, $response->status());

        $residential = Residential::where('id', $residential->id)->first();
        $this->assertEquals("RD", $residential->name);

    }

    public function test_setAdvantages()
    {
        $residential = Residential::all()->random();
        $response = $this->json('post', asset('/api/residential/advantages/set'), [
            "residential_id" => $residential->id,
            "active_advantages" => [
                "0" => 1
            ]
        ], $this->headers);
        $this->assertEquals(200, $response->status());

        $this->assertNotNull(ResidentialAdvantage::where('residential_id', $residential->id)->where('advantage_id', 1)->first());
    }

    public function test_grid()
    {
        $response = $this->json('post', asset('/api/residential/create'), [
            "coordinates" => "20.12, 40.1",
            "name" => "Residential",
            "desc" => "Cool",
            "residential_status" => 1,
            "residential_type" => 1,
            "house_class" => 2,
            "technology_build" => "Rock and Panel",
            "territory" => 1,
            "distance_sea" => 1000,
            "communal_payments" => 1,
            "height_ceiling" => 3.4,
            "gas_types" => 1,
            "heating_type" => 2,
            "sewerage_types" => 1,
            "water_types" => 1,
            "registration" => "Simply",
            "mortage" => 1,
            "flat_type" => 2,
            "sum_contract" => 10,
            "center_firstname" => "Vika",
            "center_lastname" => "Blade",
            "center_phone" => "0987836898",
            "center_email" => "vika@gmail.com"
        ], $this->headers);
        $this->assertEquals(200, $response->status());

        $residential = Residential::where('id', $response->json('residential_id'))->first();
        $post = Post::all()->random();
        $response = $this->json('post', asset('/api/residential/grid/cell/write'), [
            "residential_id" => $residential->id,
            "section" => 1,
            "row" => 2,
            "col" => 2,
            "post_id" => $post->id
        ], $this->headers);
        $this->assertEquals(200, $response->status());

        $response = $this->json('get', asset('/api/residential/grid/cell/get'), [
            "residential_id" => $residential->id,
            "section" => 1,
            "row" => 2,
            "col" => 2
        ], $this->headers);
        $this->assertEquals(200, $response->status());
        $this->assertEquals($post->id, $response->json('id'));

        $response = $this->json('post', asset('/api/residential/grid/cell/clear'), [
            "residential_id" => $residential->id,
            "section" => 1,
            "row" => 2,
            "col" => 2
        ], $this->headers);
        $this->assertEquals(200, $response->status());

        $response = $this->json('get', asset('/api/residential/grid/cell/get'), [
            "residential_id" => $residential->id,
            "section" => 1,
            "row" => 2,
            "col" => 2
        ], $this->headers);
        $this->assertEquals(300, $response->status());
    }
}
