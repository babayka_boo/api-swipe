<?php

namespace Tests\Unit;


use App\Models\Favorite;
use App\Models\Post;
use App\Models\Residential;
use App\Models\User;
use Tests\TestCase;

class PostTest extends TestCase
{
    private $headers;
    public function setUp(): void
    {
        parent::setUp();
        $this->headers = ['Authorization' => 'Bearer 1O96tGAa8rPoR4QyVL4vmtuJL7GkHyTOMyp5WxMOp7IbSuyjMd2mRVknFKBa'];
    }

    public function test_mainFunc()
    {
        $response = $this->json('get', asset('/api/post/form/create'), [], $this->headers);
        $this->assertEquals(200, $response->status());
        $this->assertNotEmpty($response->json('districts'));
        $this->assertNotEmpty($response->json('micro_districts'));
        $this->assertNotEmpty($response->json('post_type'));
        $this->assertNotEmpty($response->json('residentials'));
        $this->assertNotEmpty($response->json('document_owner'));
        $this->assertNotEmpty($response->json('flat_type'));
        $this->assertNotEmpty($response->json('plans'));
        $this->assertNotEmpty($response->json('flat_state'));
        $this->assertNotEmpty($response->json('balcony'));
        $this->assertNotEmpty($response->json('heating_type'));
        $this->assertNotEmpty($response->json('payment_types'));
        $this->assertNotEmpty($response->json('mortage'));
        $this->assertNotEmpty($response->json('connection_type'));

        $response = $this->json('post', asset('/api/post/create'), [
            "address" => "ул. Темерязева 7",
              "district" => 2,
              "micro_district" => 1,
              "post_type" => 1,
            "residential_id" => Residential::all()->random()->id,
              "document_owner" => 1,
              "flat_type" => 2,
              "count_room" => 2,
              "flat_state" => 1,
              "plans" => [
                    "0" => 1,
                    "1" => 2
              ],
              "square" => 72,
              "square_kitchen" => 20,
              "balcony" => 1,
              "heating_type" => 3,
              "commission" => 100000,
              "mortage" => 1,
              "connection_type" => 1,
              "desc" => "Flat is good",
              "price" => 34000000,
              "payment_type" => 1
        ], $this->headers);
        $this->assertEquals(200, $response->status());
        $this->assertNotNull($response->json('post_id'));
        $post = Post::where('id', $response->json('post_id'))->first();

        $this->assertEquals("ул. Темерязева 7", $post->address);
        $this->assertEquals(2, $post->district);
        $this->assertEquals(1, $post->user_id);

        $response = $this->json('post', asset('/api/post/update'), [
            'post_id' => $post->id,
            "address" => "ул. Княжеская 7",
            "district" => 1,
            "micro_district" => 1,
            "post_type" => 1,
            "residential_id" => $post->residential_id,
            "document_owner" => 1,
            "flat_type" => 2,
            "count_room" => 2,
            "flat_state" => 1,
            "plans" => [
                "0" => 1,
                "1" => 2
            ],
            "square" => 72,
            "square_kitchen" => 20,
            "balcony" => 1,
            "heating_type" => 3,
            "commission" => 100000,
            "mortage" => 1,
            "connection_type" => 1,
            "desc" => "Flat is good",
            "price" => 34000000,
            "payment_type" => 1
        ], $this->headers);
        $this->assertEquals(200, $response->status());

        $post = Post::where('id', $post->id)->first();
        $this->assertEquals("ул. Княжеская 7", $post->address);
        $this->assertEquals(1, $post->district);
        $this->assertEquals(1, $post->user_id);
    }

    public function test_access()
    {
        $post = Post::where('user_id', '!=', 1)->first();
        $response = $this->json('get', asset('/api/post/get'), [
            "post_id" => $post->id,
        ], $this->headers);
        $this->assertEquals(200, $response->status());

        $response = $this->json('post', asset('/api/post/update'), [
            "post_id" => $post->id,
        ], $this->headers);
        $this->assertEquals(301, $response->status());

    }

    public function test_changeState()
    {
        $post = Post::all()->random();

        $response = $this->json('get', asset('/api/favorite/post/add'), [
            "post_id" => $post->id,
        ], $this->headers);
        $this->assertEquals(200, $response->status());
        $response = $this->json('get', asset('/api/post/get'), [
            "post_id" => $post->id,
        ], $this->headers);
        $this->assertEquals(200, $response->status());

        $this->assertEquals($post->views + 1, Post::where('id', $post->id)->first()->views);
        $this->assertEquals($post->favorite + 1, count(Favorite::where('post_id', $post->id)->get()));


        $response = $this->json('get', asset('/api/favorite/post/delete'), [
            "post_id" => $post->id,
        ], $this->headers);
        $this->assertEquals(200, $response->status());
        $this->assertEquals($post->favorite, count(Favorite::where('post_id', $post->id)->get()));

    }

    public function test_deletePost()
    {
        $post = Post::where('user_id', '!=', 1)->first();
        $response = $this->json('post', asset('/api/post/delete'), [
            "post_id" => $post->id,
        ], $this->headers);
        $this->assertEquals(301, $response->status());
        $this->assertNotNull(Post::where('id', $post->id)->first());

        $response = $this->json('post', asset('/api/post/create'), [
            "address" => "ул. Темерязева 7",
            "district" => 2,
            "micro_district" => 1,
            "post_type" => 1,
            "residential_id" => Residential::all()->random()->id,
            "document_owner" => 1,
            "flat_type" => 2,
            "count_room" => 2,
            "flat_state" => 1,
            "plans" => [
                "0" => 1,
                "1" => 2
            ],
            "square" => 72,
            "square_kitchen" => 20,
            "balcony" => 1,
            "heating_type" => 3,
            "commission" => 100000,
            "mortage" => 1,
            "connection_type" => 1,
            "desc" => "Flat is good",
            "price" => 34000000,
            "payment_type" => 1
        ], $this->headers);
        $this->assertEquals(200, $response->status());

        $post = Post::where('user_id', 1)->first();
        $response = $this->json('post', asset('/api/post/delete'), [
            "post_id" => $post->id,
        ], $this->headers);
        $this->assertEquals(200, $response->status());
        $this->assertNull(Post::where('id', $post->id)->first());
    }

    public function test_filter()
    {
        $response = $this->json('get', asset('/api/post/list'), [
            "post_type_id" => 1
        ], $this->headers);
        $this->assertEquals(200, $response->status());

        $posts = $response->decodeResponseJson()['posts'];
        foreach ($posts as $post)
            $this->assertFalse($post['post_type_id'] != 1);

        $response = $this->json('get', asset('/api/post/list'), [
            "post_type_id" => 2
        ], $this->headers);
        $this->assertEquals(200, $response->status());

        $posts = $response->decodeResponseJson()['posts'];
        if (count($posts) > 0) {
            foreach ($posts as $post)
                $this->assertFalse($post['post_type_id'] != 2);

            $response = $this->json('post', asset('/api/filter/set'), [
                "post_type_id" => 2,
                "district" => null,
                "micro_district" => null,
                "count_room" => null,
                "price_from" => null,
                "price_to" => null,
                "square_from" => null,
                "square_to" => null,
                "flat_type" => null,
                "mortage" => 10,
                "state" => null,
                "save_filter" => 10
            ], $this->headers);
            $this->assertEquals(200, $response->status());

            $response = $this->json('get', asset('/api/post/list'), [
                "post_type_id" => 2
            ], $this->headers);
            $this->assertEquals(200, $response->status());

            $posts = $response->decodeResponseJson()['posts'];
                if (count($posts) > 0) {
                    foreach ($posts as $post)
                        $this->assertFalse($post['mortage'] != 10);
                }
            }
    }

    public function test_getPromotion()
    {
        $response = $this->json('get', asset('/api/promotion/get'), [], $this->headers);
        $this->assertEquals(200, $response->status());

        $this->assertEquals(4, count($response->decodeResponseJson()));
    }

    public function test_setPromotion()
    {
        $response = $this->json('post', asset('/api/post/create'), [
            "address" => "ул. Темерязева 7",
            "district" => 2,
            "micro_district" => 1,
            "post_type" => 1,
            "residential_id" => Residential::all()->random()->id,
            "document_owner" => 1,
            "flat_type" => 2,
            "count_room" => 2,
            "flat_state" => 1,
            "plans" => [
                "0" => 1,
                "1" => 2
            ],
            "square" => 72,
            "square_kitchen" => 20,
            "balcony" => 1,
            "heating_type" => 3,
            "commission" => 100000,
            "mortage" => 1,
            "connection_type" => 1,
            "desc" => "Flat is good",
            "price" => 34000000,
            "payment_type" => 1
        ], $this->headers);
        $post = Post::where('id', $response->json('post_id'))->first();
        $response = $this->json('post', asset('/api/promotion/set'), [
            'post_id' => $post->id,
            'promotions' => [
                '1' => '',
                '2' => 'green'
            ]
        ], $this->headers);
        $this->assertEquals(200, $response->status());

        $response = $this->json('get', asset('/api/post/list'), [], $this->headers);
        $this->assertEquals(200, $response->status());

        $this->assertEquals($post->id, $response->json('promotions')[0]['id']);
    }
}
