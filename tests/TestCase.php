<?php

namespace Tests;

use Database\Seeders\DatabaseSeeder;
use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use RefreshDatabase;

    protected $faker;

    public function setUp(): void
    {
        parent::setUp();

        //------
        $seed = new DatabaseSeeder();
        $seed->run();
        //------

        $this->faker = Factory::create();
    }
}
